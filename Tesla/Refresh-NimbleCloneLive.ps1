## <copyright file="Refresh-NimbleCloneLive.ps1" company="NimbleStorage">
## Copyright (c) 2016 All Right Reserved, http://www.nimblestorage.com/
##
## THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
## KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
## PARTICULAR PURPOSE.
##
## </copyright>
## <author>Adam Herbert</author>
## <email>aherbert@nimblestorage.com</email>
## <date>2016-11-09</date>
## <summary>Refresh clone live</summary>

$nimbleGroupIP = "rtp-afa31.rtplab.nimblestorage.com"
$nimbleUsername = "admin"
$nimblePassword = "admin"
$volumeNames = @("CloneRDM")

[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

function Main
{
  $nsArrayToken = Connect-NSArray -GroupAddress $nimbleGroupIP -UserName $nimbleUsername -Password $nimblePassword
  Write-Host "Logged into" $nimbleGroupIP

  foreach ($volumeName in $volumeNames) 
  {
    # Get Nimble volume object
    $response = Send-NSRequest -RequestMethod "GET" -RequestTarget "volumes/detail" -RequestArguments @{"name" = $volumeName; "clone" = "true"} 
    $volumeObject = $response.data

    # Get latest snapshot from parent volume
    $response = Send-NSRequest -RequestMethod "GET" -RequestTarget "snapshots/detail" -RequestArguments @{"vol_id" = $volumeObject.parent_vol_id; "sortBy" = "creation_time"}
    $parentSnaps = $response.data | ? { $_.id -ne $volumeObject.base_snap_id }
    $newParentSnap = $parentSnaps | Select-Object -Last 1

    # Try and refresh clone
    try
    {
      Write-Host "Refreshing $($volumeObject.name) with snapshot $($newParentSnap.name) from parent $($volumeObject.parent_vol_name)"
      $response = Send-NSRequest -RequestMethod "POST" -RequestTarget "volumes/$($volumeObject.id)/actions/clone_refresh" -RequestArguments @{"new_parent_snap_id" = $newParentSnap.id; "refresh_while_online" = "true"}
      $response.StatusCode
    }
    catch
    {
      Write-Warning "Couldn't refresh clone" 
      Write-Warning $_.Exception.Message
    }
  }

  # Logout
  Write-Host "Logging Out"
  $response = Send-NSRequest -RequestMethod "DELETE" -RequestTarget $("tokens/" + $nsArrayToken.id)
  $response.StatusCode
}

function Connect-NSArray 
{
    [CmdletBinding()]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $GroupAddress,

        [string]
        $UserName="admin",

        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=1)]
        [string]
        $Password
    )

    $script:nsGroupAddress = $GroupAddress

    $data = @{
        'username' = $UserName
        'password' = $Password
    }

    $return = Send-NSRequest -RequestMethod 'post' -RequestTarget 'tokens' -RequestArguments $data

    $script:nsAuthToken = $return.data.session_token

    Return $return.data

}

function Send-NSRequest
{
<#
.SYNOPSIS
Abstract the Invoke-Restmethod for all other module cmdlets

.DESCRIPTION
.PARAMETER RequestMethod
  Set the request method used. Valid options are 'get', 'delete', 'post', 'put'
.PARAMETER RequestTarget
  Set the request object and any sub object
.PARAMETER RequestArguments
  Set URI arguments for GET requests
.PARAMETER RequestHeaders
  Set any additional headers. The X-Auth-Token header is set automatically from the $site:nsAuthToken variable
.PARAMETER RequestBody
  Set the Request Body. 
.INPUTS
.OUTPUTS
.EXAMPLE
.EXAMPLE
.LINK
#>
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
        [ValidateSet('get', 'delete', 'post', 'put')]
        [string]$RequestMethod,

        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
        [string]$RequestTarget,
        
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true)]
        [hashtable]$RequestArguments,

        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true)]
        [hashtable]$RequestHeaders = @{}
    )

    if ( $script:nsAuthToken -ne $null ) { $RequestHeaders.Add("X-Auth-Token", $script:nsAuthToken) }

    $uri = 'https://' + $script:nsGroupAddress + ':5392/'
    if ($RequestTarget -eq 'versions')
    {
        $uri = $uri + 'versions'
    }
    else
    {
        $uri = $uri + 'v1/' + $RequestTarget
    }


    if ( $RequestMethod -ieq 'get' )
    {
        if ($RequestArguments.Count -gt 0)
        {
            $uri += '?'
            $uri += [string]::join("&", @(foreach($pair in $RequestArguments.GetEnumerator()) { if ($pair.Name) { $pair.Name + '=' + $pair.Value } }))
        }
    }
    else
    {
        $RequestJSON = ConvertTo-Json(@{ data = $RequestArguments })
    }

    try
    {
        $request = [System.Net.WebRequest]::Create($uri)
        $request.Method = $RequestMethod.ToUpper()
        $request.ContentType = "application/json"
        $RequestHeaders.GetEnumerator() | % { $request.Headers.Add($_.Key, $_.Value) }
        if ( $RequestMethod -ine 'GET' -and $RequestArguments.Count -gt 0 )
        {
            $requestStream = $request.GetRequestStream();
            $streamWriter = New-Object System.IO.StreamWriter $requestStream
            $streamWriter.Write($RequestJSON.ToString())
            $streamWriter.Flush()
            $streamWriter.Close()
        }
 
        $response = $request.GetResponse()
    }
    catch
    {
        $httpStatus = [regex]::matches($_.exception.message, "(?<=\()[\d]{3}").Value
        if ( $httpStatus -eq '401' )
        {
            Write-Warning 'You must login into the Nimble Array with Connect-NSArray'
            Exit
        }
        elseif ($httpStatus -ne $null)
        {
            $responseStream = $_.Exception.InnerException.Response.GetResponseStream()
            $readStream = New-Object System.IO.StreamReader $responseStream
            $data = $readStream.ReadToEnd()
            $results = $data | ConvertFrom-Json
            Throw "$('There was an error, status code: ' + $httpStatus)`n$($uri)`n$($RequestJSON.ToString())`n$($results.messages | Format-Table | Out-String)"
        }
        else
        { 
            Throw $_.Exception
        }
        Return
    }
    finally
    {
        #if ($null -ne $streamWriter) { $streamWriter.Dispose() }
        #if ($null -ne $requestStream) { $requestStream.Dispose() }
    }
    $responseStream = $response.GetResponseStream()
    $readStream = New-Object System.IO.StreamReader $responseStream
    $data = $readStream.ReadToEnd()
    #$response.Close()
    $results = New-Object -TypeName PSCustomObject
    $results | Add-Member -MemberType NoteProperty -Name StatusCode -Value "$([int]$response.StatusCode) - $($response.StatusCode)" 
    $results | Add-Member -MemberType NoteProperty -Name data -Value $($data | ConvertFrom-Json).data

    Return $results
}

Main