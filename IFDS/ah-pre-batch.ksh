#!/bin/ksh

. /usr/local/bin/ah-env.ksh

#Cleanup Old Snaps
echo "Cleanup old Batch-Start snapshots"
eval $SSHCMD admin@${ONLINEARRAY} "snapcoll --delete Batch-Start --volcoll ${VOLCOLL}"
eval $SSHCMD admin@${BATCHARRAY} "snapcoll --delete Batch-Start --volcoll ${VOLCOLL}"

#Take replicated snapshot
echo "Take new replicated snapshot while DB is frozen"
eval $SSHCMD admin@${ONLINEARRAY} "volcoll --snap ${VOLCOLL} --snapcoll_name Batch-Start --replicate"

#Thaw DB
echo "Thaw Online DB"

#Monitor replication until complete
echo "Replication in progress\c" 
REPLPROGRESS=""
while [ "$REPLPROGRESS" != "none" ] 
do
  echo " .\c" 
  REPLPROGRESS=$(${SSHCMD} admin@${ONLINEARRAY} "volcoll --info ${VOLCOLL}" | grep "Replication in-progress" | cut -d ":" -f 2 | tr -d ' ')
  sleep 10
done
echo " done!"

#Unmount and export batch vg 
echo "Unmount and export batch vg"
umount /mnt/ah-batch-vol01
vgchange -a n /dev/vgbatch
vgexport /dev/vgbatch

#Clean up old clones
echo "Remove old batch clones"
VOLUMES=$(${SSHCMD} admin@${BATCHARRAY} "volcoll --info ${VOLCOLL}" | grep "Associated volumes:" | cut -d: -f2 | tr -d ',' | xargs -n 1 | sort)
for x in $VOLUMES
  do $SSHCMD admin@${BATCHARRAY} "vol --offline $x-batch --force; vol --delete $x-batch"
done

#Rescan and remove old devices
ioscan -fnC disk | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null
ioscan -m lun | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null

#Promote on Array B
echo "Promote volumes for batch processing"
eval $SSHCMD admin@${BATCHARRAY} "volcoll --promote ${VOLCOLL}"

#Assign ACL and LUN numbers to promoted Batch volumes
echo "Assign initiator group and LUN numbers to Batch volumes"
VOLUMES=$(${SSHCMD} admin@${BATCHARRAY} "volcoll --info ${VOLCOLL}" | grep "Associated volumes:" | cut -d: -f2 | tr -d ',' | xargs -n 1 | sort)
LUN=${BATCHFIRSTLUN}
for vol in $VOLUMES; do
    $SSHCMD admin@${BATCHARRAY} 'for x in `vol --info '$vol' | grep "Initiator Group:" | cut -d : -f 2`; do vol --removeacl '$vol' --initiatorgrp $x; done'
    $SSHCMD admin@${BATCHARRAY} 'vol --addacl '$vol' --initiatorgrp '$INITGRP' --lun '$LUN
    ((LUN=LUN+1))
done

#Disable replication from Online to Batch
echo "Stop replication from Online to Batch"
eval $SSHCMD admin@${ONLINEARRAY} "volcoll --editsched ${VOLCOLL} --schedule hourly --replicate_to \\\"\\\""

#Rescan for promoted array
ioscan -fnC disk | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null
ioscan -m lun | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null
SERIALS=$(for VOL in $VOLUMES; do $SSHCMD admin@$BATCHARRAY "vol --info ${VOL}" | grep "Serial number:" | cut -d: -f2; done)
RDISKS=$(for SERIAL in $SERIALS; do scsimgr -p get_attr all_lun -a vid -a serial_number -a device_file | grep "Nimble.*$SERIAL" | cut -d: -f3; done)
DISKS=$(echo $RDISKS | sed -e 's/rdisk/disk/g')
printf "y\n" | vgchgid -f $RDISKS
vgimport -m /etc/vgmaps/vgbatch.map /dev/vgbatch $DISKS
vgchange -a y /dev/vgbatch
fsck /dev/vgbatch/ah-vol01
mount /dev/vgbatch/ah-vol01 /mnt/ah-batch-vol01

#Batch process runs
echo "Start batch process"
