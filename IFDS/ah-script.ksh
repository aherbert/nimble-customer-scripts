#!/bin/ksh

if [ "$1" = "pre" ]
then
  /usr/local/bin/ah-pre-batch.ksh
elif [ "$1" = "post" ]
then
  /usr/local/bin/ah-post-batch.ksh
fi
