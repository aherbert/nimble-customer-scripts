#!/bin/ksh

ONLINEARRAY=rtp-array59.rtplab.nimblestorage.com
ONLINENAME=group-rtp-array59
ONLINEFIRSTLUN=50
BATCHARRAY=rtp-array9.rtplab.nimblestorage.com
BATCHNAME=group-rtp-array9
BATCHFIRSTLUN=60
VOLCOLL="ah-bcv"
INITGRP="hp-ops02-E8G-grp-1"
SSHKEY="/.ssh/id_rsa"
SSHCMD="ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${SSHKEY}"

#Cleanup Old Snaps
echo "Cleanup old Batch-Start snapshots"
eval $SSHCMD admin@${ONLINEARRAY} "snapcoll --delete Batch-Start --volcoll ${VOLCOLL}"
eval $SSHCMD admin@${BATCHARRAY} "snapcoll --delete Batch-Start --volcoll ${VOLCOLL}"

#Take replicated snapshot
echo "Take new replicated snapshot while DB is frozen"
eval $SSHCMD admin@${ONLINEARRAY} "volcoll --snap ${VOLCOLL} --snapcoll_name Batch-Start --replicate"

#Thaw DB
echo "Thaw Online DB"

#Monitor replication until complete
echo "Replication in progress\c" 
REPLPROGRESS=""
while [ "$REPLPROGRESS" != "none" ] 
do
  echo " .\c" 
  REPLPROGRESS=$(${SSHCMD} admin@${ONLINEARRAY} "volcoll --info ${VOLCOLL}" | grep "Replication in-progress" | cut -d ":" -f 2 | tr -d ' ')
  sleep 10
done
echo " done!"

#Unmount and export batch vg 
echo "Unmount and export batch vg"
umount /mnt/ah-batch-vol01
vgchange -a n /dev/vgbatch
vgexport /dev/vgbatch

#Clean up old clones
echo "Remove old batch clones"
VOLUMES=$(${SSHCMD} admin@${BATCHARRAY} "volcoll --info ${VOLCOLL}" | grep "Associated volumes:" | cut -d: -f2 | tr -d ',' | xargs -n 1 | sort)
for x in $VOLUMES
  do $SSHCMD admin@${BATCHARRAY} "vol --offline $x-batch --force; vol --delete $x-batch"
done

#Rescan and remove old devices
ioscan -fnC disk | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null
ioscan -m lun | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null

#Promote on Array B
echo "Promote volumes for batch processing"
eval $SSHCMD admin@${BATCHARRAY} "volcoll --promote ${VOLCOLL}"

#Assign ACL and LUN numbers to promoted Batch volumes
echo "Assign initiator group and LUN numbers to Batch volumes"
VOLUMES=$(${SSHCMD} admin@${BATCHARRAY} "volcoll --info ${VOLCOLL}" | grep "Associated volumes:" | cut -d: -f2 | tr -d ',' | xargs -n 1 | sort)
LUN=${BATCHFIRSTLUN}
for vol in $VOLUMES; do
    $SSHCMD admin@${BATCHARRAY} 'for x in `vol --info '$vol' | grep "Initiator Group:" | cut -d : -f 2`; do vol --removeacl '$vol' --initiatorgrp $x; done'
    $SSHCMD admin@${BATCHARRAY} 'vol --addacl '$vol' --initiatorgrp '$INITGRP' --lun '$LUN
    ((LUN=LUN+1))
done

#Disable replication from Online to Batch
echo "Stop replication from Online to Batch"
eval $SSHCMD admin@${ONLINEARRAY} "volcoll --editsched ${VOLCOLL} --schedule hourly --replicate_to \\\"\\\""

#Rescan for promoted array
ioscan -fnC disk | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null
ioscan -m lun | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null
SERIALS=$(for VOL in $VOLUMES; do $SSHCMD admin@$BATCHARRAY "vol --info ${VOL}" | grep "Serial number:" | cut -d: -f2; done)
RDISKS=$(for SERIAL in $SERIALS; do scsimgr -p get_attr all_lun -a vid -a serial_number -a device_file | grep "Nimble.*$SERIAL" | cut -d: -f3; done)
DISKS=$(echo $RDISKS | sed -e 's/rdisk/disk/g')
printf "y\n" | vgchgid -f $RDISKS
vgimport -m /etc/vgmaps/vgbatch.map /dev/vgbatch $DISKS
vgchange -a y /dev/vgbatch
fsck /dev/vgbatch/ah-vol01
mount /dev/vgbatch/ah-vol01 /mnt/ah-batch-vol01

#Batch process runs
echo "Start batch process"

echo "Press enter to continue rest of script"
read cont

#Shutdown DBs
echo "It's midnight!!! R2, shutdown all the DBs on the detention level!"

#Unmount and export batch vg 
echo "Unmount and export batch vg"
umount /mnt/ah-batch-vol01
vgchange -a n /dev/vgbatch
vgexport /dev/vgbatch

#Unmount and export batch vg 
echo "Unmount and export online vg"
umount /mnt/ah-online-vol01
vgchange -a n /dev/vgonline
vgexport /dev/vgonline

#Demote on Array A
echo "Demote online volumes and re-establish replication"
eval $SSHCMD admin@${ONLINEARRAY} "volcoll --demote ${VOLCOLL} --partner ${BATCHNAME}"
eval $SSHCMD admin@${BATCHARRAY} "volcoll --editsched ${VOLCOLL} --schedule hourly --replicate_to ${ONLINENAME} --num_retain_replica 48"

#Cleanup Old Snaps
echo "Cleanup old Pre-Handover snapshots"
eval $SSHCMD admin@${ONLINEARRAY} "snapcoll --delete Pre-Handover --volcoll ${VOLCOLL}"
eval $SSHCMD admin@${BATCHARRAY} "snapcoll --delete Pre-Handover --volcoll ${VOLCOLL}"

#Take replicated snapshot
echo "Take new replicated snapshot prior to handover"
eval $SSHCMD admin@${BATCHARRAY} "volcoll --snap ${VOLCOLL} --snapcoll_name Pre-Handover --replicate"

#Monitor replication until complete
echo "Replication in progress\c" 
REPLPROGRESS=""
while [ "$REPLPROGRESS" != "none" ] 
do
  echo " .\c" 
  REPLPROGRESS=$(${SSHCMD} admin@${BATCHARRAY} "volcoll --info ${VOLCOLL}" | grep "Replication in-progress" | cut -d ":" -f 2 | tr -d ' ')
  sleep 10
done
echo " done!"

#Handover to Array A
eval $SSHCMD admin@${BATCHARRAY} "volcoll --handover $VOLCOLL --partner ${ONLINENAME}"

#Monitor replication until complete
echo "Handover in progress\c" 
REPLPROGRESS=""
while [ "$REPLPROGRESS" != "none" ] 
do
  echo " .\c" 
  REPLPROGRESS=$(${SSHCMD} admin@${BATCHARRAY} "volcoll --info ${VOLCOLL}" | grep "Replication in-progress" | cut -d ":" -f 2 | tr -d ' ')
  sleep 10
done
echo " done!"

#Assign ACL and LUN numbers to Online volumes
echo "Assign initiator group and LUN numbers to Online volumes"
VOLUMES=$(${SSHCMD} admin@${ONLINEARRAY} "volcoll --info ${VOLCOLL}" | grep "Associated volumes:" | cut -d: -f2 | tr -d ',' | xargs -n 1 | sort)
LUN=${ONLINEFIRSTLUN}
for vol in $VOLUMES; do 
    $SSHCMD admin@${ONLINEARRAY} 'for x in `vol --info '$vol' | grep "Initiator Group:" | cut -d : -f 2`; do vol --removeacl '$vol' --initiatorgrp $x; done'
    $SSHCMD admin@${ONLINEARRAY} 'vol --addacl '$vol' --initiatorgrp '$INITGRP' --lun '$LUN
    ((LUN=LUN+1))
done

#Rescan for promoted array
ioscan -fnC disk | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null
ioscan -m lun | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null
SERIALS=$(for VOL in $VOLUMES; do $SSHCMD admin@$ONLINEARRAY "vol --info $VOL" | grep "Serial number:" | cut -d: -f2; done)
RDISKS=$(for SERIAL in $SERIALS; do scsimgr -p get_attr all_lun -a vid -a serial_number -a device_file | grep "Nimble.*$SERIAL" | cut -d: -f3; done)
DISKS=$(echo $RDISKS | sed -e 's/rdisk/disk/g')
printf "y\n" | vgchgid -f $RDISKS
vgimport -m /etc/vgmaps/vgbatch.map /dev/vgonline $DISKS
vgchange -a y /dev/vgonline
fsck /dev/vgonline/ah-vol01
mount /dev/vgonline/ah-vol01 /mnt/ah-online-vol01

#Re-enable replication
echo "Re-enable hourly replication"
eval $SSHCMD admin@${ONLINEARRAY} "volcoll --editsched ${VOLCOLL} --schedule hourly --replicate_to ${BATCHNAME} --num_retain_replica 48"

#Cleanup Old Snaps
echo "Cleanup old Job-Complete snapshots"
eval $SSHCMD admin@${ONLINEARRAY} "snapcoll --delete Job-Complete --volcoll ${VOLCOLL}"
eval $SSHCMD admin@${BATCHARRAY} "snapcoll --delete Job-Complete --volcoll ${VOLCOLL}"

#Take replicated snapshot
echo "Take new replicated snapshot after handover for batch clone base"
eval $SSHCMD admin@${ONLINEARRAY} "volcoll --snap ${VOLCOLL} --snapcoll_name Job-Complete --replicate"

#Monitor replication until complete
echo "Replication in progress\c" 
REPLPROGRESS=""
while [ "$REPLPROGRESS" != "none" ] 
do
  echo " .\c" 
  REPLPROGRESS=$(${SSHCMD} admin@${ONLINEARRAY} "volcoll --info ${VOLCOLL}" | grep "Replication in-progress" | cut -d ":" -f 2 | tr -d ' ')
  sleep 10
done
echo " done!"

#Cleanup demote and handover snaps
echo "Cleanup system snapshots from demote and handover"
CMD='for x in `snapcoll --list --volcoll '${VOLCOLL}' | grep "demoted\\|handover" | tr -s " " | cut -d " " -f 2`; do snapcoll --delete $x --volcoll '${VOLCOLL}'; done'
eval $SSHCMD admin@${ONLINEARRAY} '$CMD'
eval $SSHCMD admin@${BATCHARRAY} '$CMD'

#Clone from Array B for batch DB
echo "Create clone volumes for Batch DB after midnight"
VOLUMES=$(${SSHCMD} admin@${BATCHARRAY} "volcoll --info ${VOLCOLL}" | grep "Associated volumes:" | cut -d: -f2 | tr -d ',' | xargs -n 1 | sort)
LUN=${BATCHFIRSTLUN}
for vol in $VOLUMES; do
  $SSHCMD admin@${BATCHARRAY} 'vol --clone '$vol' --clonename '$vol'-batch --snapname Job-Complete'
  $SSHCMD admin@${BATCHARRAY} 'for x in `vol --info '$vol' | grep "Initiator Group:" | cut -d : -f 2`; do vol --removeacl '$vol' --initiatorgrp $x; done'
  $SSHCMD admin@${BATCHARRAY} 'for x in `vol --info '$vol'-batch | grep "Initiator Group:" | cut -d : -f 2`; do vol --removeacl '$vol'-batch --initiatorgrp $x; done'
  $SSHCMD admin@${BATCHARRAY} 'vol --addacl '$vol'-batch --initiatorgrp '$INITGRP' --lun '$LUN
  ((LUN=LUN+1))
done

#Rescan for promoted array
ioscan -fnC disk | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null
ioscan -m lun | grep "NO_HW" | awk '{print $3}' | xargs -i -t rmsf -H {} > /dev/null
SERIALS=$(for VOL in $VOLUMES; do $SSHCMD admin@$BATCHARRAY "vol --info ${VOL}-batch" | grep "Serial number:" | cut -d: -f2; done)
RDISKS=$(for SERIAL in $SERIALS; do scsimgr -p get_attr all_lun -a vid -a serial_number -a device_file | grep "Nimble.*$SERIAL" | cut -d: -f3; done)
DISKS=$(echo $RDISKS | sed -e 's/rdisk/disk/g')
printf "y\n" | vgchgid -f $RDISKS
vgimport -m /etc/vgmaps/vgbatch.map /dev/vgbatch $DISKS
vgchange -a y /dev/vgbatch
fsck /dev/vgbatch/ah-vol01
mount /dev/vgbatch/ah-vol01 /mnt/ah-batch-vol01

#Restart Online DB
echo "Restart the DBs"
