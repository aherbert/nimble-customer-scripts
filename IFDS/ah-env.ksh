#!/bin/ksh

ONLINEARRAY=rtp-array59.rtplab.nimblestorage.com
ONLINENAME=group-rtp-array59
ONLINEFIRSTLUN=50
BATCHARRAY=rtp-array9.rtplab.nimblestorage.com
BATCHNAME=group-rtp-array9
BATCHFIRSTLUN=60
VOLCOLL="ah-bcv"
INITGRP="hp-ops02-E8G-grp-1"
SSHKEY="/.ssh/id_rsa"
SSHCMD="ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${SSHKEY}"
