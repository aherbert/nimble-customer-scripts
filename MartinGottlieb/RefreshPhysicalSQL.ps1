### CONFIG VARIABLES ###
$NimbleGroup = "rtp-afa31.rtplab.nimblestorage.com"
$NimbleUsername = "admin"
$NimblePassword = ""
$VolumeCollection = "SQLServer"
$ScheduleName = "hourly"
$InitiatorGroup = "rtp-iron-sm04"
$SQLInstance = "MSSQLSERVER"
$MountPath = "C:\nimble"
$emailFrom = "aherbert@nimblestorage.com"
$emailTo = "aherbert@nimblestorage.com"
$emailSubject = "Output of SQL refresh"
$Global:emailBody = ""
$smtpServer = "mail.nimblestorage.com"
$logMethods = @{ 
  "email" = $false
  "console" = $true
  "event" = $false
}

# Function for handling log output
Function Write-Log {
  Param(   
    [Parameter(Mandatory=$true,
    ValueFromPipelineByPropertyName=$true)]
    [string]$LogMessage,
    
    [Parameter(Mandatory=$false,
    ValueFromPipelineByPropertyName=$true)]
    [ValidateSet('Error', 'Information', 'FailureAudit', 'SuccessAudit', 'Warning')]
    [string]$LogLevel="Information"
  )
  if ($stopWatch -ne $null -and $stopWatch.GetType().Name -eq "Stopwatch") {
    $executionTime = $stopWatch.Elapsed.TotalSeconds
    $LogMessage = [string]::Format("{0,6:N2}s: {1}", $executionTime, $LogMessage)
  }
  $scriptName = & { $MyInvocation.ScriptName } | Split-Path -Leaf
  if ($logMethods["email"]) {
      $Global:emailBody += "$LogLevel : $LogMessage`n"
  }
  if ($logMethods["event"]) {
    # Create Event Log Source
    New-EventLog -LogName Application -Source $scriptName -ErrorAction SilentlyContinue
    Write-EventLog -LogName Application -EventId "1" -Source $scriptName -EntryType $LogLevel -Message "$LogMessage"
  }
  if ($logMethods["console"]) {
    if ($LogLevel -ieq "Error" -or $LogLevel -ieq "FailureAudit") {
      Microsoft.PowerShell.Utility\Write-Host -ForegroundColor Red $LogMessage
    }
    elseif ($LogMessage -ieq "Warning") {
      Microsoft.PowerShell.Utility\Write-Host -ForegroundColor Yellow $LogMessage
    }
    else {
      Microsoft.PowerShell.Utility\Write-Host $LogMessage
    }
  }
}

Function Send-LogEmail {
  if ($logMethods["email"]) {
    Send-MailMessage -SmtpServer $smtpServer -From $emailFrom -To $emailTo -Subject $emailSubject -Body $Global:emailBody
  }
} 

trap {
  $LogMessage = "Exception: $($_.Exception.Message) at line:$($_.InvocationInfo.ScriptLineNumber) char:$($_.InvocationInfo.OffsetInLine)" 
  Write-Log -LogLevel FailureAudit -LogMessage $logMessage
  $emailSubject = "FAILED! - $($emailSubject)"
  Send-LogEmail
  Exit
}
$ErrorActionPreference = "Stop"

#Disable SSL checking for communication to the array
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

# Record Script Start Time
$stopWatch = [System.Diagnostics.Stopwatch]::StartNew()

# Load Nimble Module
Import-Module NimblePowershellToolKit

# Setup Nimble group credential
if ($NimblePassword -ne "") {
  $NimbleSecurePassword = ConvertTo-SecureString -String $NimblePassword -AsPlainText -Force
} 
else {
  try {
    $NimbleSecurePassword = Get-Content "$PSScriptRoot\NimbleSecurePassword.txt" | ConvertTo-SecureString
  }
  catch {
    Read-Host "Enter Nimble Group Password" -AsSecureString | ConvertFrom-SecureString | Out-File "$PSScriptRoot\NimblePassword.txt"
    $NimbleSecurePassword = Get-Content "$PSScriptRoot\NimblePassword.txt" | ConvertTo-SecureString
  }
}
$NimbleCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $NimbleUsername, $NimbleSecurePassword

<# Shutdown SQL Services #>
$services = Get-Service | ? {$_.DisplayName -imatch "SQL Server.*$SQLInstance"}
foreach($service in $services) {
  Write-Log -LogLevel Information -LogMessage "Stopping SQL service $($service.Name)"
  Stop-Service -Force -Name $service.Name
}
<##>

# Connect to Nimble array
Write-Log -LogLevel Information -LogMessage "Connecting to $NimbleGroup"
try{
  $nimbleGroupObject = Connect-NSGroup -group $NimbleGroup -credential $NimbleCredential
  Write-Log -LogLevel Information -LogMessage "Connected to $NimbleGroup"
}
catch {
  Write-Log -LogLevel Error -LogMessage "Failed to connect to $NimbleGroup"
  Throw $_.Exception
}

<# Get Snapshot list for Volume Collection #>
$snapColl = Get-NSSnapshotCollection | ? { $_.volcoll_name -eq $VolumeCollection -and $_.sched_name -ieq $ScheduleName } | Sort-Object name | Select-Object -Last 1
Write-Log -LogLevel Information -LogMessage "Using most recent snapshot $($snapColl.name)"
Write-Log -LogLevel Information -LogMessage "Using volumes $(($snapColl.snapshots_list | Select -ExpandProperty vol_name) -join ', ')"

<# Loop through each volume snapshot to create clones #>
$cloneObjects = @()
foreach($snap in $snapColl.snapshots_list) {

  <# Set Volume Clone Variables #>
  $cloneName = $snap.vol_name + "-" + $env:COMPUTERNAME + "-" + $SQLInstance
  $cloneDescription = "Auto clone for " + $snap.vol_name + " to " + $env:COMPUTERNAME + ":" + $SQLInstance

  <# Remove old clone if still exists #>
  $oldClone = Get-NSVolume -name $cloneName | ? { $_.clone -eq $true }
  if ($oldClone -ne $null) 
  {
    Set-NSVolume -id $oldClone.id -online $false -force $true | Out-Null
    Write-Log -LogLevel Information -LogMessage "Set old volume $($oldClone.name) offline"
    $deletedName = $oldClone.name + "-" + (([char[]]([char]'A'..[char]'Z') + 0..9 | sort {get-random})[0..6] -join '')
    try { Set-NSVolume -id $oldClone.id -Name $deletedName  -ErrorAction SilentlyContinue } catch {}
    Remove-NSVolume -id $oldClone.id
    Start-Sleep -Seconds 2
    Write-Log -LogLevel Information -LogMessage "Renamed old volume $($oldClone.name) to $($deletedName) and set to delete"
  }

  <# Create new clone #>
  Write-Log -LogLevel Information -LogMessage "Create new clone of $($snap.vol_name)"
  $cloneObject = New-NSClone -name $cloneName -description $cloneDescription -base_snap_id $snap.id -clone $true -online $true
  $cloneObjects += $cloneObject

  <# Set Access Control on new clone #>
  Write-Log -LogLevel Information -LogMessage "Set access control of $($cloneObject.name) to initiator group $InitiatorGroup"
  foreach ($acl in $cloneObject.access_control_records)
  {
    Remove-NSAccessControlRecord -id $acl.acl_id
  }
  $igroupObject = Get-NSInitiatorGroup -name $InitiatorGroup
  $result = New-NSAccessControlRecord -apply_to "both" -initiator_group_id $igroupObject.id -vol_id $cloneObject.id
}


<# Rescan for new devices #>
Write-Log -LogLevel Information -LogMessage "Rescanning for new drives"
"rescan" | diskpart | out-null
Start-Sleep -Seconds 30
<##>

<# Mount new Clones  #>
foreach ($cloneObject in $cloneObjects) { 
  Write-Log -LogLevel Information -LogMessage "Setting $($cloneObject.name) online"
  $disk = Get-Disk | ? { $_.SerialNumber -eq $cloneObject.serial_number }
  Set-Disk -Number $disk.Number -isOffline $false
  Set-Disk -Number $disk.Number -isReadOnly $false

  $partitionList = Get-Partition -DiskNumber $disk.Number -ErrorAction SilentlyContinue
  if ($partitionList.Size -eq 0) {
    Write-Warning "No mountable partitions on $($cloneObject.name)"
  }
  foreach ($partition in $partitionList) {
    $volume = Get-Volume -Partition $partition
    if ($volume -eq $null) {
      Write-Log -LogLevel Information -LogMessage "No filesystem on disk $($disk.Number) partition $($partition.PartitionNumber)"
      Continue
    }
    Write-Log -LogLevel Information -LogMessage "Setting partition $($partition.PartitionNumber) online for volume $($volume.FileSystemLabel)"
    Set-Partition -DiskNumber $disk.Number -PartitionNumber $partition.PartitionNumber -IsReadOnly $false -IsHidden $false -NoDefaultDriveLetter $true
    $volumePath = "\" + $cloneObject.name + "\" + $partition.PartitionNumber
    mountvol $($MountPath + $volumePath) /D
    mkdir $($MountPath + $volumePath) -ErrorAction SilentlyContinue | Out-Null
    foreach ($accessPath in $partition.AccessPaths | ? { $_ -notmatch "Recycle" -and $_ -notmatch "\\\\?\\" }) {
      Remove-PartitionAccessPath -DiskNumber $disk.Number -PartitionNumber $partition.PartitionNumber -AccessPath $accessPath
    }
    Write-Log -LogLevel Information -LogMessage "Adding mount point $($MountPath + $volumePath)"
    Add-PartitionAccessPath -DiskNumber $disk.Number -PartitionNumber $partition.PartitionNumber -AccessPath $($MountPath + $volumePath)
  }
}
<##>

<# Restart SQL Services #>
foreach($service in $services) {
  if((Get-WmiObject Win32_Service -filter ("Name='"+$service.Name+"'")).StartMode -eq "Auto") {
    Write-Log -LogLevel Information -LogMessage "Starting SQL service $($service.Name)"
    Start-Service -Name $service.Name
  }
}
<##>

$executionTime = [math]::round($stopWatch.Elapsed.TotalSeconds, 2)
Write-Log -LogLevel Information -LogMessage "Finished in $executionTime seconds!"
Send-LogEmail