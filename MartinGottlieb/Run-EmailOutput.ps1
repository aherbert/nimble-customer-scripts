$from = "sysadmin@gottlieb.com"
$to = "sysadmin#gottlieb.com"
$subject = "Output of - $($args[0])"
$smtpServer = "smtp.gottlieb.com"

$body = "Output of - $($args[0])"
$body += "-"
$body += Invoke-Command { $args[0] }

$body = $body | out-string

$email = @{
  "From" = $from
  "To" = $to
  "Subject" = $subject
  "SMTPServer" = $smtpServer
  "Body" = $body
}

Send-MailMessage $email