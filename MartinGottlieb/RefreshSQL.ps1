### CONFIG VARIABLES ###
$NimbleGroup = "rtp-afa31.rtplab.nimblestorage.com"
$NimbleUsername = "admin"
$NimblePassword = "admin"
$VolumeCollection = "SQLServer"
$ScheduleName = "hourly"
$InitiatorGroup = "rtp-iron-sm04"
$VCenterServer = "sa-vcsa.rtplab.nimblestorage.com"
$VCenterUsername = "administrator@salab.local"
$VCenterPassword = "Nim123Boli!"
$TargetVM = "Herbert-Win2k12"
$SQLInstance = "MSSQLSERVER"
$MountPath = "C:\nimble"

# Load Nimble Module
Import-Module NimblePowershellToolKit

# Load VMware Module
try {
  Add-PSSnapin VMware.VimAutomation.Core -ErrorAction Stop
}
catch {
  Import-Module VMware.VimAutomation.Core
}

# Use password above for PSCredential Object
$NimbleSecurePassword = ConvertTo-SecureString -String $NimblePassword -AsPlainText -Force
$NimbleCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $NimbleUsername, $NimbleSecurePassword

# Connect to Nimble array
$nimbleGroupObject = Connect-NSGroup -group $NimbleGroup -credential $NimbleCredential

# Use password above for PSCredential Object
$VCenterSecurePassword = ConvertTo-SecureString -String $VCenterPassword -AsPlainText -Force
$VCenterCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $VCenterUsername, $VCenterSecurePassword

# Connect to VCenter Server
$viServerObject = Connect-VIServer -Server $VCenterServer -Credential $VCenterCredential

<# Get Target VM Object #>
$targetVMObject = Get-VM -Name $TargetVM
<##>

<# Shutdown SQL Services #>
$services = Get-Service | ? {$_.DisplayName -imatch "SQL Server.*$SQLInstance"}
foreach($service in $services) {
  Write-Host "Stopping SQL service " $service.Name
  Stop-Service -Force -Name $service.Name
}
<##>

<# Get Snapshot list for Volume Collection #>
$snapColl = Get-NSSnapshotCollection | ? { $_.volcoll_name -eq $VolumeCollection -and $_.sched_name -ieq $ScheduleName } | Sort-Object name | Select-Object -Last 1
Write-Host "Using most recent snapshot "$snapColl.name" with the following volumes:"
$snapColl.snapshots_list | Format-Table vol_name

<# Loop through each volume snapshot to create clones #>
$cloneObjects = @()
foreach($snap in $snapColl.snapshots_list) {

  <# Set Volume Clone Variables #>
  $cloneName = $snap.vol_name + "-" + $TargetVM + "-" + $SQLInstance
  $cloneDescription = "Auto clone for " + $snap.vol_name + " to " + $TargetVM + ":" + $SQLInstance

  <# Remove old clone if still exists #>
  $oldClone = Get-NSVolume -name $cloneName | ? { $_.clone -eq $true }
  if ($oldClone -ne $null) 
  {
    mountvol $($MountPath + "\" + $cloneName) /d
    Get-HardDisk -vm $targetVMObject -DiskType RawPhysical | Where-Object `
      { $_.ScsiCanonicalName -eq "eui." + $oldClone.serial_number } | Remove-HardDisk -Confirm:$false -DeletePermanently
    Set-NSVolume -id $oldClone.id -online $false -force $true | Out-Null
    Write-Host "Set old volume " $oldClone.name " offline"
    Remove-NSVolume -id $oldClone.id
    Write-Host "Deleted old volume " $oldClone.name
  }
  
  <# Create new clone #>
  $cloneObject = New-NSClone -name $cloneName -description $cloneDescription -base_snap_id $snap.id -clone $true -online $true
  $cloneObjects += $cloneObject

  <# Set Access Control on new clone #>
  foreach ($acl in $cloneObject.access_control_records)
  {
    Remove-NSAccessControlRecord -id $acl.acl_id
  }
  $igroupObject = Get-NSInitiatorGroup -name $InitiatorGroup
  $result = New-NSAccessControlRecord -apply_to "both" -initiator_group_id $igroupObject.id -vol_id $cloneObject.id
}

<# Refresh ESX host for new volumes #>
Write-Host "Refreshing to find new targets"
$targetVMObject.VMHost | Get-VMHostStorage -RescanAllHba -RescanVmfs | Out-Null
<##>

<# Connect to volume clones to VM as RDM #>
foreach ($cloneObject in $cloneObjects) { 
  $ScsiLun = Get-ScsiLun -VmHost $targetVMObject.VMHost | Where-Object { $_.CanonicalName -eq "eui." + $cloneObject.serial_number }
  $rdmDisk = New-HardDisk -vm $targetVMObject -DiskType RawPhysical -DeviceName $ScsiLun.ConsoleDeviceName
}
<##>

<# Rescan for new devices #>
Write-Host "Rescanning for new drives"
"rescan" | diskpart | out-null
Start-Sleep -Seconds 10
<##>

<# Mount new RDMs in VM  #>
foreach ($cloneObject in $cloneObjects) { 
  Write-Host "Setting " $cloneObject.name " online"
  $disk = Get-Disk | ? { $_.SerialNumber -eq $cloneObject.serial_number }
  Set-Disk -Number $disk.Number -isOffline $false
  Set-Disk -Number $disk.Number -isReadOnly $false
  Set-Partition -DiskNumber $disk.Number -PartitionNumber $disk.NumberOfPartitions -IsReadOnly $false -IsHidden $false -NoDefaultDriveLetter $true
  mkdir $($MountPath + "\" + $cloneObject.name) -ErrorAction SilentlyContinue | Out-Null
  mountvol $($MountPath + "\" + $cloneObject.name) /d
  Add-PartitionAccessPath -DiskNumber $disk.Number -PartitionNumber $disk.NumberOfPartitions -AccessPath $($MountPath + "\" + $cloneObject.name)
}
<##>

<# Restart SQL Services #>
foreach($service in $services) {
  if((Get-WmiObject Win32_Service -filter ("Name='"+$service.Name+"'")).StartMode -eq "Auto") {
    Write-Host "Starting SQL service " $service.Name
    Start-Service -Name $service.Name
  }
}
<##>

<# Rescan the whole cluster #>
Write-Host "Rescanning Cluster for new clones" 
Get-Cluster | Get-VMHost | Get-VMHostStorage -RescanAllHba -RescanVmfs
<##>

<# Disconnect from Nimble Group and viServer #>
Disconnect-NSGroup
Disconnect-VIServer -Confirm:$false
<##>

Write-Host "Finished!"