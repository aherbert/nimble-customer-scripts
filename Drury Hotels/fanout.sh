#!/bin/bash

set -e

ARRAY=10.18.128.50
TARGET1="TMSandbox"
TARGET2="mktg-loaner1"
VOLCOLL="AH-Test"
SSHKEY="/Users/aherbert/.ssh/nimble"
SSHCMD="ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null admin@${ARRAY} -i ${SSHKEY}"

REMOTESNAPS=$(${SSHCMD} "volcoll --info ${VOLCOLL}" | grep "Number of snapshots to retain on replica:" | cut -d : -f 2 | tr -d ' ') #| cut -c 44-)
REPLPROGRESS=$(${SSHCMD} "volcoll --info ${VOLCOLL}" | grep "Replication in-progress" | cut -d ":" -f 2 | tr -d ' ')
if [ "$REPLPROGRESS" != "none" ] 
then 
  echo "Replication in progress" 
  exit 1
fi
OLDTARGET=$(${SSHCMD} "volcoll --info ${VOLCOLL}" | grep "Replicate to:" | cut -d : -f 2 | tr -d ' ') # | cut -c 16-)

if [ "$OLDTARGET" = "$TARGET1" ]
then
  NEWTARGET=$TARGET2
else
  NEWTARGET=$TARGET1
fi

if [ "0$REMOTESNAPS" -gt 0 ] 
then 
  ${SSHCMD} "volcoll --editsched ${VOLCOLL} --schedule fanout --replicate_to \"\""
fi
sleep 2
${SSHCMD} "volcoll --editsched ${VOLCOLL} --schedule fanout --replicate_to ${NEWTARGET} --num_retain_replica ${REMOTESNAPS:-99}"
