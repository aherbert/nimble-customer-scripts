<#################################################################################################
.SYNOPSIS
Nimble PowerShell Toolkit

.DESCRIPTION
Ths module Parses attributes for each command-let from NimblePowerShellToolKit.xml and prepare a REST requeest and execute

.COPYRIGHT
Copyright 2015 Nimble Storage, Inc.

.AUTHORS
Ravindranadh C Sahukara (ravin@nimblestorage.com)

.DATE
May 2015

.MODIFIED
July 2015
#################################################################################################>
#Requires -Version 3

$NimbleToolkitArrtibuteFile = "$PSScriptRoot\NimblePowerShellToolKit.xml"
if((Test-Path -Path $NimbleToolkitArrtibuteFile) -eq $false) {
    Write-Error "Nimble ToolKit Attributes file $NimbleToolkitArrtibuteFile doesn't Exist"
    Exit 2
}

#Loads XML Attibute data file
$nwtConfig = [xml] (get-content -Path $NimbleToolkitArrtibuteFile)

$restport = 5392

$NimbleToolkitDesiredAttributesFile = "$PSScriptRoot\NimblePowerShellToolKitDesiredFields.json"
if((Test-Path -Path $NimbleToolkitDesiredAttributesFile) -eq $false) {
    Write-Error "Nimble ToolKit Attributes file $NimbleToolkitDesiredAttributesFile doesn't Exist"
    Exit 2
}

$NimbleToolkitDesiredAttributes = (Get-Content -Path $NimbleToolkitDesiredAttributesFile) | Out-String | ConvertFrom-Json

#region certificate stuff
#This toolkit retrives the SSL certificate before any other communication
#Cache the certificate in memory
function Get-Certificate {
   param ([Parameter(Mandatory=$true)] [System.Uri] $Uri,
         [Parameter()] [Switch] $TrustAllCertificates)

  try {
    $request = [System.Net.WebRequest]::Create($Uri)
    if($request.servicepoint.certificate) {
        return $request.servicepoint.certificate
    }

    if ($TrustAllCertificates) {
      # Create a compilation environment
      $Provider=New-Object Microsoft.CSharp.CSharpCodeProvider
      $Compiler=$Provider.CreateCompiler()
      $Params=New-Object System.CodeDom.Compiler.CompilerParameters
      $Params.GenerateExecutable=$False
      $Params.GenerateInMemory=$True
      $Params.IncludeDebugInformation=$False
      $Params.ReferencedAssemblies.Add("System.DLL") > $null

      $TASource=@'
        namespace Local.ToolkitExtensions.Net.CertificatePolicy {
          public class TrustAll : System.Net.ICertificatePolicy {
            public TrustAll() { 
            }
            public bool CheckValidationResult(System.Net.ServicePoint sp,
              System.Security.Cryptography.X509Certificates.X509Certificate cert, 
              System.Net.WebRequest req, int problem) {
              return true;
            }
          }
        }
'@ 
      $TAResults=$Provider.CompileAssemblyFromSource($Params,$TASource)
      $TAAssembly=$TAResults.CompiledAssembly

      ## We now create an instance of the TrustAll and attach it to the ServicePointManager
      $TrustAll=$TAAssembly.CreateInstance("Local.ToolkitExtensions.Net.CertificatePolicy.TrustAll")
      [System.Net.ServicePointManager]::CertificatePolicy=$TrustAll
    }

    $response = $request.GetResponse()
    $servicePoint = $request.ServicePoint
    $certificate = $servicePoint.Certificate
    return $certificate

  } catch {
    Write-Error "Failed to get website certificate. The error was '$_'."
    return $null
  }

}

#Comptes Epoh Time and returns
function Get-EpochTime {
    $inceptionDate = get-date -Date '01/01/1970'
    $today = get-date
    $timeSpan = New-TimeSpan -Start $inceptionDate -End $today
    $epoch_time_sec = $timeSpan.TotalSeconds
    $epoch_time_sec = [math]::Round($epoch_time_sec)
    return $epoch_time_sec

}
#endregion certificate stuff

#return Object information, which is operations and its arguments
function getObjectsConfig {
    $objects = ($nwtConfig.NsToolKitConfig.Objects)
    return $objects
}

#Returns filter and action query templates
function getTemplateConfig {
    $templConfig = ($nwtConfig.NsToolKitConfig.templates)
    return $templConfig
}

#Returns Object Names like volume, snapshot, versions etc
function getObjectNames {
    $objectsConfig = getObjectsConfig
    $objectNames = @(($objectsConfig | Get-Member -MemberType Properties | Select-Object name).name)
    return $objectNames
}

#returns Operations associated with a particular object
function getOperations {
    Param([Parameter (mandatory = $true)] [string] $objName)
    $objects = getObjectsConfig
    $operations = @((($objects.$objName.'operations') | Get-Member -MemberType Properties | Select-Object name).name)
    return $operations
}

#Returns Object operation attributes/arguments
function getAttributes {
    Param([Parameter (mandatory = $true)] $objName,
          [Parameter (mandatory = $true)] $operation)
    $objectsConfig = getObjectsConfig
    $attributes = @($objectsConfig.$objName.'operations'.$operation.'parameters'.'parameter')
    return $attributes
}

#Returns operations which have filter templates
function getTemplateOperations {
    $templateConfig = getTemplateConfig
    $templOperations = @(($templateConfig | Get-Member -MemberType Properties | Select-Object name).name)
    return $templOperations
}

#return Templates for a given operation
function getTemplates {
    Param([Parameter (mandatory = $true)] $operation)
    $templateConfig = getTemplateConfig
    $templates = @($templateConfig.$operation.'template') |Sort-Object length -Descending
    return $templates
}

#return Uniq set of attributes/arguments for a given operation
function getUniqTemplateFields {  # Need to work more on this
    Param([Parameter (mandatory = $true)] $operation)
    $tmplFields = @()
    $fields = @()
    $templateConfig = getTemplateConfig
    $templates = @($templateConfig.$operation.'template')
    foreach ($template in $templates) {
        #$template = $template.replace('{ObjectSet}', '')
        $fields += @([regex]::Matches($template, '{\w+}') | %{$k = $_.value.replace('{','');$k.replace('}','')})

    }
    $tmplFields = $fields | Sort-Object -Unique
    return $tmplFields
}

#Parse the template and return filter attributes
function getUniqFieldsofTemplate { 
    Param([Parameter (mandatory = $true)] $template)
    $tmplFields = @()
    $fields = @()
    #$template = $template.replace('{ObjectSet}', '')
    $fields += @([regex]::Matches($template, '{\w+}') | %{$k = $_.value.replace('{','');$k.replace('}','')})
    $tmplFields = $fields | Sort-Object -Unique
    return $tmplFields
}

#Object Original Name, This is needed because the Cmd-lets uses singular-name
function getOriginalName {
    Param([Parameter (mandatory = $true)] $objName)
    $objectsConfig = getObjectsConfig
    $originalName = $objectsConfig.$objName.'origname'
    return $originalName
}

#Check given set of attributes exits in cmd-let provided arguments
function isFieldsExist {
    Param([Parameter(mandatory = $true)] $fields,
          [Parameter(mandatory = $false)] $argsHash)
    $existFlag = $false
    if($argsHash -eq $null) {
        return $existFlag
    }
    foreach ($field in $fields) {
        if($argsHash.$field) {
            $existFlag = $true
        }

    }
    return $existFlag
}

#Check Does all attributes exists in cmd-let provided arguments
function isAllFieldsExist {
    Param([Parameter(mandatory = $true)] $fields,
          [Parameter(mandatory = $false)] $argsHash)
    $existFlag = $true
    if($argsHash -eq $null) {
        return $false
    }
    foreach ($field in $fields) {
        if($argsHash.$field -eq $null) {
            $existFlag = $false
        }

    }
    return $existFlag
}

#Returns Object operation output default attributes/arguments
function getDefaultProperties {
    Param([Parameter (mandatory = $true)] $objName,
          [Parameter (mandatory = $true)] $operation)
    $objectsConfig = getObjectsConfig
    $attributes = @($objectsConfig.$objName.'operations'.$operation.'DefaultProperties'.'property')
    return $attributes
}



#Hash Map between Nimble operations and PowerShell cmd-let Verbs
$oprHash = @{'new' = 'create'; 'set' = 'update'; 'remove' = 'delete'; 'get' = 'read'}



function getDesiredAttributes {
    Param([parameter(mandatory = $true)] $objName)
    $desiredAttributes = $NimbleToolkitDesiredAttributes.'objects'.$objName
    $desiredAttributesArray  = $desiredAttributes.split(',')
    return $desiredAttributesArray
}

function getDefaultPropertiesObject {
    $caller = (Get-PSCallStack)[1].command
    $oper = $caller.split('-')[0]
    $funcname = $caller.split('-')[1]
    $objname = $funcname.subString(2)
    $objname = getOriginalName $objname
               
    $defaultProperties = getDesiredAttributes $objName
    if($defaultProperties) {
        $defaultDisplayPropertySet = New-Object System.Management.Automation.PSPropertySet('DefaultDisplayPropertySet', [string[]]$defaultProperties)
        $PSStandardMembers = [System.Management.Automation.PSMemberInfo[]]@($defaultDisplayPropertySet)
        return $PSStandardMembers
    }
    return $null
}


function getObjectArguments {
    $caller = (Get-PSCallStack)[1].command
    $oper = $caller.split('-')[0]
    $funcname = $caller.split('-')[1]
    $objname = $funcname.subString(2)
    if($oprHash.$oper) {
        $oh = $oprHash.$oper
    } else {
        $oh = $oper
    }
    return getAttributes -objName $objname -operation $oh
}

function getObjNameAndAct {
    Param([Parameter(mandatory = $true)] $objname)
    #Write-Host "Action operation $oper"
    $objectsConfig = getObjectsConfig
    $objectNamesList = ($objectsConfig | Get-Member -MemberType Properties | Select-Object name).name

    for($i = 0; $i -lt $objname.Length; $i++) {
        $subStringIndex = ($objname.Length - $i)
        $objSubString = $objname.Substring(0, $subStringIndex )
        #Write-Host "Substring $objSubString"
        if($objectNamesList -contains $objSubString) {
            #write-host "$objSubString Available"
            $lastMachIndex = $subStringIndex
            break
        }
    }

    $oh = $objname.Substring($lastMachIndex)
        
    $objname = $objname.Substring(0, $lastMachIndex)
    #Write-Host "Objname: $objname, $operation : $oh"
    return($objname, $oh)
}
function getObjectActArguments {
    $caller = (Get-PSCallStack)[1].command
    $oper = $caller.split('-')[0]
    $funcname = $caller.split('-')[1]
    $objname = $funcname.subString(2)
    ($objname, $oh) = getObjNameAndAct $objname
    return getAttributes -objName $objname -operation $oh
}

#Helper function to print the hash, just for debug.
function printHash {
    Param([parameter(mandatory = $true)] $hashVar)
    Write-Host "============== Printing Hash =============================="
    if($hashVar -eq $null) {
        write-host "HashVar is $null"
        return $null
    }
    $keys = $hashVar.keys
    foreach ($key in $keys) {
        write-host "KEY: $key VALUE: $($hashVar.$key)"
    }
    Write-Host "============== Printing Hash Done =========================="
}



#This function called for every exception to print the appropriate error message
function PrintError {
    Param([Parameter(mandatory = $false)] $exception, $message)
    $objectName = (Get-PSCallStack)[1].command
    if($exception.Exception -is [System.Net.WebException]) {
        try { 
            $responseStreem = $exception.Exception.Response.GetResponseStream()
            if($responseStreem) {
                $responseStreem.Position = 0
                $streamReader = New-Object System.IO.StreamReader($responseStreem)
                $errMessages = $streamReader.ReadToEnd()
                $streamReader.Close()
                $responseStreem.Close()
                $exceptionMessageObjs = ($errMessages |  convertfrom-json).messages
                $exceptionMessage = ''
                foreach($excepMsg in $exceptionMessageObjs) {
                    $exceptionMessage += $excepMsg.text
                }
                Write-Host $exceptionMessage -ForegroundColor Red -BackgroundColor White
            }

        } catch {
            Write-Host "Exception Object Processing Error"
        }
    } else {
        if($exception) {
            Write-Host $exception.exception.message -ForegroundColor Red -BackgroundColor White
        } else { 
            Write-Host $message -ForegroundColor Red -BackgroundColor White
        }
    }
    throw $exception
}

#Composed the REST Header and returns the same
function getHeader {

    $header = @{}
    $token = $global:session_token | Out-String
    $header.Add("X-Auth-Token", $global:session_token)
    return $header

}

#Hash map for Rest Operations and Web request Verbs
$CRUD_Hash = @{'read' = 'GET'; 'update' = 'PUT'; 'delete' = 'DELETE'; 'create' = 'POST'}

#This function send rest request to Nimble Group
function ExecuteRest {
    Param([Parameter(mandatory = $true)] [alias("u")] $uri,
          [Parameter(mandatory = $false)] [alias("b")] $body)

    $caller = (Get-PSCallStack)[1].command
    $index = $caller.IndexOf('Ns')
    if($index -ge 0) { #Bypssing restversion call
        $opr = $caller.Substring(0, $index)
        $action = $CRUD_Hash.$opr
    }

    #Return $null if Global $array variable is not set
    if($array -eq $null) {
        $errMsg = @"
You are not connected to any group
Please connect the group with Connect-NSGroup command let
"@
         Write-Host $errMsg -BackgroundColor white -ForegroundColor Red
         Break
    }

    $header = getHeader

    $hdr = $header | out-string
    if(-not ($header.'X-Auth-Token')) {
        $header = $null
    }

    if($header) { 
        $hdr = $header | Out-String
        write-debug "Header    : $hdr"
    }

    write-debug "Caller    : $caller"
    write-debug "Operation : $opr"
    write-debug "Action    : $action"
    write-debug "Uri       : $uri"
    write-debug "Body      : $body"

    $restOut = $null
    if($body -and $header) {
        write-debug 'Executing RestMethod with body and header'
        $restOut = Invoke-RestMethod -Uri $uri -Method $action -Body $body -Headers $header
    } elseif($body -and (!$header)) {
        write-debug 'Executing RestMethod with body'
        $restOut = Invoke-RestMethod -Uri $uri -Method $action -Body $body 
    } elseif($header -and (!$body)) {
        write-debug 'Executing RestMethod with header'
        $restOut = Invoke-RestMethod -Uri $uri -Method $action -Headers $header
    } else { #Getting Rest Version
        $restOut = Invoke-RestMethod -Uri $uri 
    }
    return $restOut.data
}

#Composes a Appropriate Url based on arguments
function getRestUrl {
    Param([Parameter(mandatory = $false)] [alias("o")] $caller,
          [Parameter(mandatory = $false)] [alias("a")] $restArgs)


    $funcname = $caller.split('-')[1]
    $objname = $funcname.subString(2)
    $objname = getOriginalName $objname
    $operation = $caller.split('-')[0]
    #printHash $restArgs

    if(-not $oprHash.$operation) {
        $actionTmpl = getTemplates 'action'
        $actionTmpl = $actionTmpl.replace('{id}',$restArgs.'id') 
        $actionTmpl = $actionTmpl.replace('{action}',$operation.tolower()) 
        $actionTmpl = $actionTmpl.trim()
        $actionUrl = "https://{0}:{1}/{2}/{3}{4}" -f $array, $restport, $Global:RestVersion, $objname, $actionTmpl
        $restArgs.remove('id') | out-null
        return $actionUrl
    }
    $filterTemplate = $null
    $snapshotTemplate = $null
    if($oprHash.$operation -eq 'read') {

        if(($objname -eq 'snapshots') -and ($restArgs.'id' -eq $null)) {
            $filterKeys = @($restArgs.keys)
            $snapshotTemplate = "?{0}={1}" -f $filterKeys[0], $restArgs.($filterKeys[0])
        }

        $fields = getUniqTemplateFields $oprHash.$operation
        $isArgsExist = isFieldsExist $fields $restArgs
        if($isArgsExist) {
            $queryTmpls = getTemplates $oprHash.$operation
            foreach ($tmpl in $queryTmpls) {
                $fields = getUniqFieldsofTemplate $tmpl
                $isArgsExist = isAllFieldsExist $fields $restArgs
                if($isArgsExist) {

                    foreach ($field in $fields) {
                        $tmpl = $tmpl.replace("{$field}",$restArgs.$field)
                        $restArgs.remove($field) |out-null
                    }
                    $filterTemplate = $tmpl.trim()
                }
            }

        }
    }

    if($restArgs.'id') {
        $restUrl = "https://{0}:{1}/{2}/{3}/{4}" -f $array, $restport, $Global:RestVersion, $objname, $restArgs.'id'
        $restArgs.remove('id') | out-null
    } elseif ($restArgs.'name' -and ($oprHash.$operation -eq 'read')) {
        $restUrl = "https://{0}:{1}/{2}/{3}/detail?name={4}" -f $array, $restport, $Global:RestVersion, $objname, $restArgs.'name'
        $restArgs.remove('name') | out-null            
     } else {
        if($snapshotTemplate) {
            $restUrl = "https://{0}:{1}/{2}/{3}{4}" -f $array, $restport, $Global:RestVersion,$objname, $snapshotTemplate
            if($filterTemplate) { $filterTemplate = $filterTemplate.replace('?','&') }
        } else {
            if(($oprHash.$operation -eq 'read') -and ($filterTemplate -eq $null)) {
                $restUrl = "https://{0}:{1}/{2}/{3}/detail" -f $array, $restport, $Global:RestVersion,$objname
            } else {
                $restUrl = "https://{0}:{1}/{2}/{3}" -f $array, $restport, $Global:RestVersion,$objname
            }
        }
        if($filterTemplate) {
            $restUrl = "{0}{1}" -f $restUrl, $filterTemplate
        }

    }
    return $restUrl
}

#Composes rest body with arguments provided to Cmd-let
function getRestBody { 
    Param($argsHash)
    $bodyHash = @{}
    $tmpArgsHash = @{}
    foreach($k in $argsHash.keys) {
        if($argsHash.$k -eq $false) {

        } elseif(($argsHash.$k -eq $null) -or ($argsHash.$k -eq 0)) {
        #} elseif(($argsHash.$k -eq $null) -or ($argsHash.$k -eq '') -or ($argsHash.$k -eq 0)) {
            continue
        }
        $tmpArgsHash[$k] = $argsHash.$k
        $val = $argsHash.$k
        $valtype = ($val.GetType()).name
    }
    $bodyHash.Add("data", $tmpArgsHash)
    $body = $bodyHash | ConvertTo-Json
    return $body

}


#region Composing Arguments dynamically
#Dynamically composes arguments dictonary with parameter name, type, mandatory (yes/no), accept from pipe line etc.
function New-DynamicParam {
    param([string] $Name,
        [System.Type] $Type = [string],
        [string[]] $Alias = @(),
        [string[]] $ValidateSet,
        [switch] $Mandatory,
        [string] $ParameterSetName="__AllParameterSets",
        [int] $Position,
        [switch] $ValueFromPipelineByPropertyName,
        [switch] $ValueFromPipeline,
        [string] $HelpMessage,
        [validatescript({if(-not ( $_ -is [System.Management.Automation.RuntimeDefinedParameterDictionary] -or -not $_) ) {
        Throw "DPDictionary must be a System.Management.Automation.RuntimeDefinedParameterDictionary object, or not exist"
        } $True})]
        $DPDictionary = $false
        )

    #Create attribute object, add attributes, add to collection
    $ParamAttr = New-Object System.Management.Automation.ParameterAttribute
    $ParamAttr.ParameterSetName = $ParameterSetName

    if($mandatory) {
        $ParamAttr.Mandatory = $True
    }
    if($Position -ne $null) {
        $ParamAttr.Position=$Position
    }
    $caller = (Get-PSCallStack)[1].command

    if($Type -eq $null) {

        $Type = [String]
    }
    if($ValueFromPipelineByPropertyName) {
        $ParamAttr.ValueFromPipelineByPropertyName = $True
    }
    if($HelpMessage) {
        $ParamAttr.HelpMessage = $HelpMessage
    }
    if($ValueFromPipeline) {
        $ParamAttr.ValueFromPipeline = $ValueFromPipeline
    }
    $AttributeCollection = New-Object 'Collections.ObjectModel.Collection[System.Attribute]'
    $AttributeCollection.Add($ParamAttr)
    #param validation set if specified
    if($ValidateSet) {
        $ParamOptions = New-Object System.Management.Automation.ValidateSetAttribute -ArgumentList $ValidateSet
        $AttributeCollection.Add($ParamOptions)
    }
    #Aliases if specified
    if($Alias.count -gt 0) {
        $ParamAlias = New-Object System.Management.Automation.AliasAttribute -ArgumentList $Alias
        $AttributeCollection.Add($ParamAlias)
    }
    #Create the dynamic parameter
    $Parameter = New-Object -TypeName System.Management.Automation.RuntimeDefinedParameter -ArgumentList @($Name, $Type, $AttributeCollection)
    #Add the dynamic parameter to an existing dynamic parameter dictionary, or create the dictionary and add it
    if($DPDictionary) {
        $DPDictionary.Add($Name, $Parameter)
    } else {
        $Dictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        $Dictionary.Add($Name, $Parameter)
        $Dictionary
    }
}

#endregion Composing Arguments dynamically

#Composes arguments dictonary with cmd-let arguments
function composeParams {
    Param($ObjAttributes)

    $Dictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
    foreach ($attr in $ObjAttributes) {
        if($attr.required -eq 'True') {
            $required = $true
        } else {
            $required = $false
        }

        if($attr.required -eq 'True') {
            New-DynamicParam -Name $attr.name -DPDictionary $Dictionary -ValueFromPipelineByPropertyName -Type $attr.type -Mandatory
        } else {
            New-DynamicParam -Name $attr.name -DPDictionary $Dictionary -ValueFromPipelineByPropertyName -Type $attr.type
        }
    }
    return $Dictionary
}

function ConvertTimeStamp {
    Param([Parameter(mandatory = $true)] $obj)
    $properties = @('last_modified', 'creation_time','time','last_modified_time','last_login','last_logout')
    foreach($property in $properties) {
        foreach($o in $obj) {
            if($o.$property) {
                $o.$property = [System.TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($o.$property))
            }
        }
    }
}

#region CRUD Operations
#Process create object request. Accept arguments and values from pipeline as well

function createNsActObject {
    Param([Parameter(mandatory = $true)] $funcargs)
    Begin {
        $objectName = (Get-PSCallStack)[1].command
        $object = $objectName.split('-')[1]
        Write-Debug "Creating $object (s) ..."
        $objname = $object.subString(2)
        ($objname, $opr) = getObjNameAndAct $objname
        $objectName = "{0}-NS{1}" -f $opr, $objname

    }

    Process {
        $data = $null
        $url = getRestUrl -o $objectName -a $funcargs
        $jsonBody = getRestBody $funcargs
        $data = ExecuteRest -u $url -b $jsonBody
        if($data) {
            ConvertTimeStamp $data
        }
        return $data
    }

    End {
        write-debug "Successfully created $object $($funcargs.'name')"
    }

}

function createNsObject {
    Param([Parameter(mandatory = $true)] $funcargs)
    Begin {
        $objectName = (Get-PSCallStack)[1].command
        $object = $objectName.split('-')[1]
        Write-Debug "Creating $object (s) ..."

    }

    Process {
        $data = $null
        $url = getRestUrl -o $objectName -a $funcargs
        $jsonBody = getRestBody $funcargs
        $data = ExecuteRest -u $url -b $jsonBody
        if($data) {
            ConvertTimeStamp $data
        }
        return $data
    }

    End {
        write-debug "Successfully created $object $($funcargs.'name')"
    }

}

#Process read object request. Accept arguments and values from pipeline as well
function readNsObject {
    Param([Parameter(mandatory = $true)] $funcargs)

    Begin {
        $caller = (Get-PSCallStack)[1].command
        $NsObjType = ($caller.split('-'))[1]
        $objectType = $NsObjType.Substring(2,($NsObjType.Length - 2))
        Write-Debug "Getting $objectType (s) information..."
    }

    Process {
        $data = $null
        $objectName = $null
        if ($funcargs['id'] -ne $null) {
            $testid = $funcargs['id']
            Write-Debug "Getting $objectType information by id"
            $url = getRestUrl -a $funcargs -o $caller
            $data = ExecuteRest -u $url
        } else { 
            if($funcargs['name'] -ne $null) { 
                $objectName = $funcargs['name']
            }
            Write-Debug "Getting $objectType information"
            $url = getRestUrl -o $caller -a $funcargs
            $data = ExecuteRest -u $url
        }
        if($data -ne $null) {
            ConvertTimeStamp $data
        } 
        return $data
    }

    End {

    }
}

#Process update object request. Accept arguments and values from pipeline as well
function updateNsObject {
    Param([Parameter(mandatory = $true)] $funcargs)

    Begin {
        $caller = (Get-PSCallStack)[1].command
        $NsObjType = ($caller.split('-'))[1]
        $objectType = $NsObjType.Substring(2,($NsObjType.Length - 2))
        Write-Debug "Updating $objectType (s) information..."
    }

    Process {
        $data = $null
        if ($funcargs['id'] -ne $null) {
            write-debug "Updating $objectType by id"
            $url = getRestUrl -o $caller -a $funcargs
            $jsonBody = getRestBody $funcargs
            $data = ExecuteRest -u $url -b $jsonBody
        }

        if($funcargs['name'] -ne $null) {
            $objectName = $funcargs['name']
            write-debug "Updating $objectType by name"
            $getNsObjByName = '{0}-{1} -name {2}' -f 'Get', $NsObjType, $objectName
            $data = Invoke-Expression -Command $getNsObjByName
            if($data -ne $null) { 
                $funcargs.add('id', $data.id)
                $url = getRestUrl -a $funcargs -o $caller
                $funcargs.Remove('name') | out-null
                $jsonBody = getRestBody $funcargs
                $data = ExecuteRest -u $url -b $jsonBody
            } else {
                $objectNotFoundMessage = "Server has not found the requested object ($objectType $objectName)"
                #Write-Host $objectNotFoundMessage -BackgroundColor White -ForegroundColor Red
                $objectNotFoundException = New-Object System.Exception($objectNotFoundMessage)
                throw $objectNotFoundException            
            }
        }

        if($data -ne $null) {
            ConvertTimeStamp $data
        }
        return $data
    }

    End {
        
    }
}

#Process delete object request. Accept arguments and values from pipeline as well
function deleteNsObject {
    Param([Parameter(mandatory = $true)] $funcargs)

    Begin {
        $caller = (Get-PSCallStack)[1].command
        $NsObjType = ($caller.split('-'))[1]
        $objectType = $NsObjType.Substring(2,($NsObjType.Length - 2))
        Write-Debug "Deleting $objectType (s)..."
    }

    Process {
        $data = $null
        if ($funcargs['id'] -ne $null) {
            write-debug "Try to delete a $objectType by id"
            $url = getRestUrl -a $funcargs  -o $caller
            $funcargs.Remove('name') | out-null
            $data = ExecuteRest -u $url
        } 

        if (($funcargs['name'] -ne $null)) {
            write-debug "Try to delete a $objectType by name"
            $objectName = $funcargs['name']
            $getNsObjByName = '{0}-{1} -name {2}' -f 'Get', $NsObjType, $funcargs['name']
            $data = Invoke-Expression -Command $getNsObjByName

            if($data -ne $null) { 
                $funcargs.add('id', $data.id)
                $funcargs.Remove('name') | out-null
                $url = getRestUrl -a $funcargs -o $caller
                $data = ExecuteRest -u $url
            } else {
                $objectNotFoundMessage = "Server has not found the requested object ($objectType $objectName)"
                #Write-Host $objectNotFoundMessage -BackgroundColor White -ForegroundColor Red
                $objectNotFoundException = New-Object System.Exception($objectNotFoundMessage)
                throw $objectNotFoundException    
            }

        }
        if($data -ne $null) {
            ConvertTimeStamp $data
        }
        return $data
    }

    End {
        
    }
}
#endregion CRUD Operations

#Retrives REST server supported versions
function getRestApiVersion {
    Param([Parameter(mandatory = $true)] [String] $uri) 

    $url = "{0}/versions" -f $uri
    try {
        $webClient = New-Object System.Net.WebClient
        $response = $webClient.DownloadString($url) | ConvertFrom-Json
    } catch {
        throw $_
    }
    return $response.data.name
}

#This is the first command let to execute.
#This tetrives the session token and caches it for further use


#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml
function Connect-NSGroup {
    Param([Parameter(mandatory = $true)] [alias("g")] $group,
          [Parameter(mandatory = $true)] [alias("c")] [PSCredential] $credential)

    $url = "https://{0}:5392" -f $group
    $connObj = New-Object PsObject

    try {
        Test-Connection -ComputerName $group -Count 3 -ErrorAction Stop | Out-Null
    } catch [system.net.NetworkInformation.PingException] {
        PrintError $_ "Unable to Reach Group $group"

    } catch {
        PrintError $_ "Unknow error occured while pinging $group"
    }

    $cert = Get-Certificate -Uri $url -TrustAllCertificates

    try {
        $version = getRestApiVersion -uri $url
    } catch {
        PrintError $_ 'Unable to get REST API Version'
    }

    if($version -ne 'v1') {
        $exceptionObj = New-Object System.NotSupportedException("Server REST API Version $version Not Supported by this NimblePowerShellToolkit")
        PrintError $exceptionObj "This toolkit do not support $version of Rest Server"
    }

    Set-Variable -Name Global:RestVersion -Value $version
    Set-Variable -Name Global:array -Value $group

    #Validate Credential object type
    $credType = $credential.GetType().Name
    if($credType -ne 'PSCredential') {
        Write-Error 'Credential is not a PSCredential object type' -ErrorAction Stop
        Break
    }


    $netWorkCred = $credential.GetNetworkCredential()
    try { 
        $tokenData = new-nstoken -username $netWorkCred.UserName -password $netWorkCred.Password -app_name 'NimblePowerSehllToolkitV1' 
        Set-Variable -name Global:session_token -Value $tokenData.session_token
        Set-Variable -name Global:tokenData -Value $tokenData
        $NsArray = Get-NSArray
    } catch [System.Net.WebException] {
        PrintError $_ 'Invalid Credentials: username or password is incorrect'
    } catch [System.Exception] {
        PrintError $_ 'Unknow Exception occured'
    }

    if($group -match '\d+\.\d+\.\d+\.\d+') {
        $connObj | Add-Member -MemberType NoteProperty -Name Group -Value $NsArray.name
        $connObj | Add-Member -MemberType NoteProperty -Name ManagementIP -Value $group 
    } else {
        $connObj | Add-Member -MemberType NoteProperty -Name Group -Value $group 
    }

    $connObj | Add-Member -MemberType NoteProperty -Name User -Value $netWorkCred.UserName
    $connObj | Add-Member -MemberType NoteProperty -Name Model -Value $NsArray.model
    $connObj | Add-Member -MemberType NoteProperty -Name SerialNo -Value $NsArray.serial
    $connObj | Add-Member -MemberType NoteProperty -Name Version -Value $NsArray.version
    return $connObj
}



#This is the last command let to execute
#Removes session token from rest server and clears local cache data

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml
function Disconnect-NSGroup {
    $tokenData = $global:tokenData
    if($tokenData) {
        Remove-NSToken -id $tokenData.id
    }
    if($global:tokenData) {
        Clear-Variable -Name tokenData
    }
    if($global:session_token) {
        Clear-Variable -Name session_token
    }
    if($global:array) {
        Clear-Variable -Name array
    }
    if($global:RestVersion) {
        Clear-Variable -Name RestVersion   
    }
}

Export-ModuleMember Connect-NSGroup, Disconnect-NSGroup




#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSAccessControlRecord { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSAccessControlRecord 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSAccessControlRecord { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSAccessControlRecord 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSAccessControlRecord { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSAccessControlRecord 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSArray { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSArray 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSAuditLog { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSAuditLog 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSChapUser { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSChapUser 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSChapUser { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSChapUser 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSChapUser { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSChapUser 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSChapUser { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSChapUser 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSDebugLog { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSDebugLog 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSFibreChannelPort { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSFibreChannelPort 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSGroup { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSGroup 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSInitiatorGroup { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSInitiatorGroup 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSInitiatorGroup { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSInitiatorGroup 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSInitiatorGroup { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSInitiatorGroup 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSInitiatorGroup { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSInitiatorGroup 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSInitiator { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSInitiator 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSInitiator { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSInitiator 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSInitiator { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSInitiator 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSJob { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSJob 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSNetworkConfig { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSNetworkConfig 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSNetworkConfig { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSNetworkConfig 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSNetworkConfig { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSNetworkConfig 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSNetworkConfig { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSNetworkConfig 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSNetworkInterface { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSNetworkInterface 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSPerformancePolicy { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSPerformancePolicy 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSPerformancePolicy { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSPerformancePolicy 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSPerformancePolicy { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSPerformancePolicy 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSPerformancePolicy { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSPerformancePolicy 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSPool { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSPool 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSPool { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSPool 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSPool { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSPool 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSPool { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSPool 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Merge-NSPool { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Merge-NSPool 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSProtectionSchedule { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSProtectionSchedule 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSProtectionSchedule { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSProtectionSchedule 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSProtectionSchedule { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSProtectionSchedule 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSProtectionSchedule { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSProtectionSchedule 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSProtectionTemplate { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSProtectionTemplate 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSProtectionTemplate { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSProtectionTemplate 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSProtectionTemplate { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSProtectionTemplate 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSProtectionTemplate { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSProtectionTemplate 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSReplicationPartner { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSReplicationPartner 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSReplicationPartner { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSReplicationPartner 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSReplicationPartner { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSReplicationPartner 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSReplicationPartner { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSReplicationPartner 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSRole { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSRole 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSSnapshotCollection { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSSnapshotCollection 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSSnapshotCollection { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSSnapshotCollection 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSSnapshotCollection { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSSnapshotCollection 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSSnapshotCollection { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSSnapshotCollection 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSSnapshot { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSSnapshot 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSSnapshot { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSSnapshot 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSSnapshot { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSSnapshot 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSSnapshot { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSSnapshot 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSSnapshotCksum { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectActArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsActObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Get-NSSnapshotCksum 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSSoftwareVersion { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSSoftwareVersion 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSSubnet { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSSubnet 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSToken { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSToken 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSToken { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSToken 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSToken { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSToken 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSUser { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSUser 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSUser { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSUser 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSUser { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSUser 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSUser { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSUser 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSVolumeCollection { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSVolumeCollection 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSVolumeCollection { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSVolumeCollection 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSVolumeCollection { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSVolumeCollection 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSVolumeCollection { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSVolumeCollection 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Invoke-NSVolumeCollectionPromote { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectActArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsActObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Invoke-NSVolumeCollectionPromote 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Invoke-NSVolumeCollectionDemote { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectActArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsActObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Invoke-NSVolumeCollectionDemote 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSVolumeFamily { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSVolumeFamily 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Get-NSVolume { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = readNsObject $PSBoundParameters
            $defaultProperties = getDefaultPropertiesObject
            if(($defaultProperties -ne $null) -and ($data.PSStandardMembers -eq $null)) {
                $data | Add-Member MemberSet PSStandardMembers $defaultProperties
            }
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Get-NSVolume 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSVolume { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSVolume 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function New-NSClone { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember New-NSClone 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Set-NSVolume { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = updateNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }


    End {

    }
}
Export-ModuleMember Set-NSVolume 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Remove-NSVolume { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = deleteNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Remove-NSVolume 

#.ExternalHelp NimblePowerShellToolKit.psm1-Help.xml 
function Restore-NSVolume { 
    [CmdletBinding()]
    Param ()
 
    DynamicParam
    {
        $vattrs = getObjectArguments
        $Dictionary = composeParams $vattrs
        $Dictionary
    }

    Begin {
        $data = $null
    }

    Process {
        try {
            $data = createNsObject $PSBoundParameters
        } catch {
            PrintError $_
        }
        return $data
    }

    End {

    }
}
Export-ModuleMember Restore-NSVolume 

