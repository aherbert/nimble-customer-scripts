<#
## <copyright file="Refresh-CustomerData.ps1" company="NimbleStorage">
## Copyright (c) 2016 All Right Reserved, http://www.nimblestorage.com/
##
## THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
## KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
## PARTICULAR PURPOSE.
##
## </copyright>
## <author>Adam Herbert</author>
## <email>aherbert@nimblestorage.com</email>
## <date>2017-01-03</date>
## <summary>Custom POC script for Cotiviti to demonstrate customer 
## refresh of data from staging server. The script will create a new snapshot
## of the staging (DCMNT) volume and attach to the production (DSFLD) server
## as X:\Customer\Data. The old mount point will be remounted as 
## X:\Customer\Data_Last. The old Data_Last volume will be discarded.
##
## On the remote host you must first run Set-ExecutionPolicy -ExecutionPolicy bypass
## and Enable-PSRemoting -force
##</summary>

.SYNOPSIS
The script will create a new snapshot of the staging (DCMNT) volume 
and attach to the production (DSFLD) server

.DESCRIPTION
.PARAMETER CustomerName
  Set the customer name to be refreshed
.INPUTS
.OUTPUTS
.EXAMPLE
  Refresh-CustomerData -CustomerName "Clone" -TargetHost "localhost"
.LINK
#>

param(
  [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
  [string]$CustomerName,
  [Parameter(Mandatory=$false,ValueFromPipelineByPropertyName=$true)]
  [string]$CloneHost,
  [Parameter(Mandatory=$false,ValueFromPipelineByPropertyName=$true)]
  [string]$CloneInitGrp
)

<#
Configuration Options
<##>
#$nimbleGroup = "10.1.81.96"
#$nimbleUsername = "nimble"
$cloneRoot = "C:\$CustomerName\DSFLD"
if ($CloneHost -eq "") { $CloneHost = $env:COMPUTERNAME }
if ($CloneInitGrp -eq "") { $CloneInitGrp = $CloneHost }
$nimbleGroup = "rtp-afa31.rtplab.nimblestorage.com"
$nimbleUsername = "admin"
$emailFrom = "aherbert@nimblestorage.com"
$emailTo = "aherbert@nimblestorage.com"
$emailSubject = "Output of refresh for $CustomerName"
$Global:emailBody = ""
$smtpServer = "mail.nimblestorage.com"
$logMethods = @{ 
  "email" = $true
  "console" = $false
  "event" = $false
}
<##>

$stopWatch = [System.Diagnostics.Stopwatch]::StartNew()

#Disable SSL checking for communication to the array
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

$PSScriptRoot = Get-Location
$PSCommand = $MyInvocation.MyCommand.Name

Import-Module "$PSScriptRoot\NimblePowerShellToolKit\NimblePowerShellToolKit.psm1"
. "$PSScriptRoot\NimbleRESTFunctions.ps1"

New-EventLog -LogName Application -Source "Refresh-$CustomerName" -ErrorAction SilentlyContinue

Function Write-Log {
  Param(   
    [Parameter(Mandatory=$true,
    ValueFromPipelineByPropertyName=$true)]
    [string]$LogMessage,
    
    [Parameter(Mandatory=$false,
    ValueFromPipelineByPropertyName=$true)]
    [ValidateSet('Error', 'Information', 'FailureAudit', 'SuccessAudit', 'Warning')]
    [string]$LogLevel="Information"
  )
  if ($logMethods["email"]) {
      $Global:emailBody += "$LogLevel : $LogMessage`n"
  }
  if ($logMethods["event"]) {
    Write-EventLog -LogName Application -EventId "1" -Source "Refresh-$CustomerName" -EntryType $LogLevel -Message "$LogMessage"
  }
  if ($logMethods["console"]) {
    if ($LogLevel -ieq "Error" -or $LogLevel -ieq "FailureAudit") {
      Microsoft.PowerShell.Utility\Write-Error $LogMessage
    }
    elseif ($LogMessage -ieq "Warning") {
      Microsoft.PowerShell.Utility\Write-Warning $LogMessage
    }
    else {
      Microsoft.PowerShell.Utility\Write-Output $LogMessage
    }
  }
}

Function Send-LogEmail {
  Send-MailMessage -SmtpServer $smtpServer -From $emailFrom -To $emailTo -Subject $emailSubject -Body $Global:emailBody
}

trap {
  Write-Log -LogLevel FailureAudit -LogMessage $_
  $emailSubject = "FAILED! - $($emailSubject)"
  Send-LogEmail
  Exit
}
$ErrorActionPreference = "Stop"

# Setup Nimble group credential 
try {
  $nimblePassword = Get-Content "$PSScriptRoot\password.txt" | ConvertTo-SecureString
}
catch {
  Read-Host "Enter Password" -AsSecureString | ConvertFrom-SecureString | Out-File "$PSScriptRoot\password.txt"
  $nimblePassword = Get-Content "$PSScriptRoot\password.txt" | ConvertTo-SecureString
}
$credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $nimbleUsername, $nimblePassword

try{
  $result = Connect-NSGroup -group $nimbleGroup -credential $credential
  Write-Log -LogLevel Information -LogMessage "Connect to $nimbleGroup"
}
catch {
  Write-Log -LogLevel Error -LogMessage "Failed to connect to $nimbleGroup"
  Throw $_.Exception
}

# Set common variables
try {
  Write-Log -LogLevel Information -LogMessage "Root path for volume mounts $cloneRoot"
  $parentVolume = Get-NSVolume -name $CustomerName
  if ($parentVolume -ne $null) { Write-Log -LogLevel Information -LogMessage "Parent volume is $($parentVolume.name)" }
  $dataVolumeName = "$($CustomerName)-Data"
  $dataLastVolumeName = "$($CustomerName)-DataLast"
  $oldDataVolume = Get-NSVolume -name $dataVolumeName | Where-Object { $_.clone -eq $true }
  if ($oldDataVolume -ne $null) { Write-Log -LogLevel Information -LogMessage "Found old Data volume $($oldDataVolume.name)" }
  $oldDataLastVolume = Get-NSVolume -name $dataLastVolumeName | Where-Object { $_.clone -eq $true }
  if ($oldDataLastVolume -ne $null) { Write-Log -LogLevel Information -LogMessage "Found old DataLast volume $($oldDataLastVolume.name)" }
  $cloneInitGrpObj = Get-NSInitiatorGroup -name $cloneInitGrp
  if ($cloneInitGrpObj.id -eq $null) { Throw "Can't find $cloneInitGrp initiator group!" }
}
catch {
  Write-Log -LogLevel Error -LogMessage $_.Exception.Message
  Throw $_.Exception
}

<# To be tested on customer system #>
# Unmount Data and Data_Last in $cloneRoot
function unmountOrCreatePath {
  param([string]$Path)
  $output = Invoke-Command -ComputerName $CloneHost -ScriptBlock {
    $Path = $Using:Path
    if(Test-Path -Path $Path -PathType Container) {
      Write-Output "Unmounting $Path"
      mountvol $Path /d | Out-Null
    }
    else {
      Write-Output "Creating $Path"
      New-Item $Path -ItemType Directory | Out-Null
    }
  }
  Write-Log -LogLevel Information -LogMessage $output 
}

Write-Log -LogLevel Information -LogMessage "Unmounting or creating new path for Data clone"
unmountOrCreatePath -Path "$($cloneRoot)\Data"
Write-Log -LogLevel Information -LogMessage "Unmounting or creating new path for Data_Last clone"
unmountOrCreatePath -Path "$($cloneRoot)\Data_Last"
<##>

#Remove old clone if there was a Data_Last to remove
if ($oldDataLastVolume -ne $null) 
{
  Write-Log -LogLevel Information -LogMessage "Setting old volumes $($oldDataLastVolume.name) and $($oldDataVolume.name) offline"
  $result = Set-NSVolume -id $oldDataVolume.id -online $false -force $true
  $result = Set-NSVolume -id $oldDataLastVolume.id -online $false -force $true
  Write-Log -LogLevel Information -LogMessage "Deleted old volume $($oldDataLastVolume.name) and base snapshot"
  $result = Remove-NSVolume -id $oldDataLastVolume.id
  $result = Remove-NSSnapshot -id $oldDataLastVolume.base_snap_id
}

if ($oldDataVolume -ne $null) 
{
  Write-Log -LogLevel Information -LogMessage "Rename old Data volume to DataLast"
  $result = Set-NSVolume -id $oldDataVolume.id -online $false -force $true
  $result = Set-NSVolume -id $oldDataVolume.id -name $dataLastVolumeName -description "Renamed to $dataLastVolumeName on $(Get-Date); $($oldDataVolume.description)"
  $result = Set-NSVolume -id $oldDataVolume.id -online $true
  # This is just a safety check to be sure there isn't a DataLast snapshot
  # preventing a naming collision
  $oldDataSnapshot = Get-NSSnapshot -vol_id $parentVolume.id | ? { $_.name -ieq "DataLast" }
  if ($oldDataSnapshot -ne $null)
  {
    Write-Log -LogLevel Information -LogMessage "Removing DataLast snaphsot on $($parentVolume.name)"
    $result = Remove-NSSnapshot -id $oldDataSnapshot.id
  }
  Write-Log -LogLevel Information -LogMessage "Rename old Data snapshot to DataLast"
  $result = Send-NSRequest -RequestMethod put -RequestTarget "snapshots/$($oldDataVolume.base_snap_id)" -RequestArguments @{ name = "DataLast" }
  
  Write-Log -LogLevel Information -LogMessage "Refresh ACLs on new DataLast volume"
  foreach ($acl in $oldDataVolume.access_control_records)
  {
    $result = Remove-NSAccessControlRecord -id $acl.acl_id
  }
  $result = New-NSAccessControlRecord -vol_id $oldDataVolume.id -apply_to "both" -initiator_group_id $cloneInitGrpObj.id
}

# This is just a safety check to be sure there isn't a Data snapshot
# preventing a naming collision
$oldDataSnapshot = Get-NSSnapshot -vol_id $parentVolume.id | ? { $_.name -ieq "Data" }
if ($oldDataSnapshot -ne $null)
{
  Write-Log -LogLevel Information -LogMessage "Removing Data snaphsot on $($parentVolume.name)"
  $result = Remove-NSSnapshot -id $oldDataSnapshot.id
}
Write-Log -LogLevel Information -LogMessage "Create snapshot for new Data volume"
$snapObject = New-NSSnapshot -name "Data" -vol_id $parentVolume.id
Write-Log -LogLevel Information -LogMessage "Creating clone for new Data volume"
$newDataVolume = New-NSClone -name $dataVolumeName -description "Cloned for $($CustomerName) on $(Get-Date)" -base_snap_id $snapObject.id -clone $true -online $true
$oldDataVolume = Get-NSVolume -name $dataLastVolumeName | Where-Object { $_.clone -eq $true }
Write-Log -LogLevel Information -LogMessage "Remove inherited ACLs from Data volume"
foreach ($acl in $newDataVolume.access_control_records)
{
  $result = Remove-NSAccessControlRecord -id $acl.acl_id
}
Write-Log -LogLevel Information -LogMessage "Set ACLs on Data volume to $($cloneInitGrpObj.name) initiator group"
$result = New-NSAccessControlRecord -vol_id $newDataVolume.id -apply_to "both" -initiator_group_id $cloneInitGrpObj.id

if ($newDataVolume.target_name -match "iqn.*") {
  $output = Invoke-Command -ComputerName $CloneHost -ScriptBlock {
    Write-Output "Refreshing iSCSI targets"
    $(Get-WmiObject -Namespace root/wmi MSiSCSIInitiator_MethodClass).RefreshTargetList()
    Write-Output "Connecting to $($Using:newDataVolume.name)"
    iscsicli qlogintarget $($Using:newDataVolume.target_name)
    Write-Output "Connecting to $($Using:oldDataVolume.name)"
    iscsicli qlogintarget $($Using:oldDataVolume.target_name)
  }
  $output -split "`r`n" | % { if ($_ -ne "") { Write-Log -LogLevel Information -LogMessage "$_"  } }
}

<# To be tested on Customer System #>
Write-Log -LogLevel Information -LogMessage "Rescanning for new drives"
Invoke-Command -ComputerName $CloneHost -ScriptBlock { "rescan `n exit" | diskpart } | out-null
Start-Sleep -Seconds 30

$diskDevices = Get-WmiObject -Class Win32_DiskDrive -Namespace 'root\CIMV2' -ComputerName $CloneHost

$newDataDevice = $diskDevices | Where-Object { $_.SerialNumber -eq $newDataVolume.serial_number }
if (!$newDataDevice) {
  Throw "Couldn't find new Data device!"
}
Invoke-Command -ComputerName $CloneHost -ScriptBlock {
@"
select disk $($Using:newDataDevice.index)
attributes disk clear readonly noerr
online disk
"@ | diskpart | Out-Null

Start-Sleep -Seconds 10

@"
select disk $($Using:newDataDevice.index)
select partition 2
attributes volume clear readonly noerr
attributes volume clear hidden noerr
attributes volume clear shadowcopy noerr
assign mount=$($Using:cloneRoot)/Data
"@ | diskpart | Out-Null

label /MP "$($Using:cloneRoot)/Data" "$($Using:CustomerName)-Data"
}


$oldDataDevice = $diskDevices | Where-Object { $_.SerialNumber -eq $oldDataVolume.serial_number }
if (!$oldDataDevice) {
  Write-Log -LogLevel Warning -LogMessage "Couldn't find old Data device. Skipping mounting DataLast."
}
else {
  Invoke-Command -ComputerName $CloneHost -ScriptBlock {
@"
select disk $($Using:oldDataDevice.index)
attributes disk clear readonly noerr
online disk
"@ | diskpart | Out-Null

Start-Sleep -Seconds 10

@"
select disk $($Using:oldDataDevice.index)
select partition 2
attributes volume clear readonly noerr
attributes volume clear hidden noerr
attributes volume clear shadowcopy noerr
assign mount=$($Using:cloneRoot)/Data_Last
"@ | diskpart | Out-Null
  label /MP "$($Using:cloneRoot)/Data_Last" "$($Using:CustomerName)-DataLast"
  }
}

$executionTime = $stopWatch.Elapsed.TotalSeconds
Write-Log -LogLevel Information "Success! Completed in $executionTime seconds"

if($logMethods["email"]) { 
  $emailSubject = "Success! - $($emailSubject)"
  Send-LogEmail 
}