function Send-NSRequest
{
<#
.SYNOPSIS
Abstract the Invoke-Restmethod for all other module cmdlets

.DESCRIPTION
.PARAMETER RequestMethod
  Set the request method used. Valid options are 'get', 'delete', 'post', 'put'
.PARAMETER RequestTarget
  Set the request object and any sub object
.PARAMETER RequestArguments
  Set URI arguments for GET requests
.PARAMETER RequestHeaders
  Set any additional headers. The X-Auth-Token header is set automatically from the $global:session_token variable
.PARAMETER RequestBody
  Set the Request Body. 
.INPUTS
.OUTPUTS
.EXAMPLE
.EXAMPLE
.LINK
#>
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
        [ValidateSet('get', 'delete', 'post', 'put')]
        [string]$RequestMethod,

        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
        [string]$RequestTarget,
        
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true)]
        [hashtable]$RequestArguments,

        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true)]
        [hashtable]$RequestHeaders = @{}
    )

    if ( $global:session_token -ne $null ) { $RequestHeaders.Add("X-Auth-Token", $global:session_token) }

    $uri = 'https://' + $global:array + ':5392/'
    if ($RequestTarget -eq 'versions')
    {
        $uri = $uri + 'versions'
    }
    else
    {
        $uri = $uri + 'v1/' + $RequestTarget
    }


    if ( $RequestMethod -ieq 'get' )
    {
        if ($RequestArguments.Count -gt 0)
        {
            $uri += '?'
            $uri += [string]::join("&", @(foreach($pair in $RequestArguments.GetEnumerator()) { if ($pair.Name) { $pair.Name + '=' + $pair.Value } }))
        }
    }
    else
    {
        $RequestJSON = ConvertTo-Json(@{ data = $RequestArguments })
    }

    try
    {
        $request = [System.Net.WebRequest]::Create($uri)
        $request.Method = $RequestMethod.ToUpper()
        $request.ContentType = "application/json"
        $RequestHeaders.GetEnumerator() | % { $request.Headers.Add($_.Key, $_.Value) }
        if ( $RequestMethod -ine 'GET' -and $RequestArguments.Count -gt 0 )
        {
            $requestStream = $request.GetRequestStream();
            $streamWriter = New-Object System.IO.StreamWriter $requestStream
            $streamWriter.Write($RequestJSON.ToString())
            $streamWriter.Flush()
            $streamWriter.Close()
        }
 
        $response = $request.GetResponse()
    }
    catch
    {
        $httpStatus = [regex]::matches($_.exception.message, "(?<=\()[\d]{3}").Value
        if ( $httpStatus -eq '401' )
        {
            Write-Warning 'You must login into the Nimble Array with Connect-NSGroup'
            Exit
        }
        elseif ($httpStatus -ne $null)
        {
            $responseStream = $_.Exception.InnerException.Response.GetResponseStream()
            $readStream = New-Object System.IO.StreamReader $responseStream
            $data = $readStream.ReadToEnd()
            $results = $data | ConvertFrom-Json
            Throw "$('There was an error, status code: ' + $httpStatus)`n$($uri)`n$($RequestJSON.ToString())`n$($results.messages | Format-Table | Out-String)"
        }
        else
        { 
            Throw $_.Exception
        }
        Return
    }
    finally
    {
        #if ($null -ne $streamWriter) { $streamWriter.Dispose() }
        #if ($null -ne $requestStream) { $requestStream.Dispose() }
    }
    $responseStream = $response.GetResponseStream()
    $readStream = New-Object System.IO.StreamReader $responseStream
    $data = $readStream.ReadToEnd()
    #$response.Close()
    $results = New-Object -TypeName PSCustomObject
    $results | Add-Member -MemberType NoteProperty -Name StatusCode -Value "$([int]$response.StatusCode) - $($response.StatusCode)" 
    $results | Add-Member -MemberType NoteProperty -Name data -Value $($data | ConvertFrom-Json).data

    Return $results
}