#!/bin/ksh

ONLINEARRAY=rtp-array59.rtplab.nimblestorage.com
ONLINENAME=group-rtp-array59
ONLINEFIRSTLUN=50
BATCHARRAY=rtp-array9.rtplab.nimblestorage.com
BATCHNAME=group-rtp-array9
BATCHFIRSTLUN=60
VOLCOLL="ah-bcv"
INITGRP="hp-ops02-E8G-grp-1"
SSHKEY="/.ssh/id_rsa"
SSHCMD="ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${SSHKEY}"

#### DR Fail Over steps

#Promote on Array B
echo "Promote volumes for batch processing"
eval $SSHCMD admin@${BATCHARRAY} "volcoll --promote ${VOLCOLL}"

#Assign ACL and LUN numbers to promoted Batch volumes
echo "Assign initiator group and LUN numbers to Batch volumes"
VOLUMES=$(${SSHCMD} admin@${BATCHARRAY} "volcoll --info ${VOLCOLL}" | grep "Associated volumes:" | cut -d: -f2 | tr -d ',' | xargs -n 1 | sort)
for vol in $VOLUMES; do
    $SSHCMD admin@${BATCHARRAY} 'for x in `vol --info '$vol' | grep "Initiator Group:" | cut -d : -f 2`; do vol --removeacl '$vol' --initiatorgrp $x; done'
    $SSHCMD admin@${BATCHARRAY} 'vol --addacl '$vol' --initiatorgrp '$INITGRP
done

#### Attach volumes in the DR SQL Servers

#### Fail Back 

#Demote on Array A
echo "Demote online volumes and re-establish replication"
eval $SSHCMD admin@${ONLINEARRAY} "volcoll --demote ${VOLCOLL} --partner ${BATCHNAME}"
eval $SSHCMD admin@${BATCHARRAY} "volcoll --editsched ${VOLCOLL} --schedule hourly --replicate_to ${ONLINENAME} --num_retain_replica 48"

#Handover to Array A
eval $SSHCMD admin@${BATCHARRAY} "volcoll --handover $VOLCOLL --partner ${ONLINENAME}"

#Monitor replication until complete
echo "Handover in progress\c" 
REPLPROGRESS=""
while [ "$REPLPROGRESS" != "none" ] 
do
  echo " .\c" 
  REPLPROGRESS=$(${SSHCMD} admin@${BATCHARRAY} "volcoll --info ${VOLCOLL}" | grep "Replication in-progress" | cut -d ":" -f 2 | tr -d ' ')
  sleep 10
done
echo " done!"

#Assign ACL and LUN numbers to Online volumes
echo "Assign initiator group and LUN numbers to Online volumes"
VOLUMES=$(${SSHCMD} admin@${ONLINEARRAY} "volcoll --info ${VOLCOLL}" | grep "Associated volumes:" | cut -d: -f2 | tr -d ',' | xargs -n 1 | sort)
for vol in $VOLUMES; do 
    $SSHCMD admin@${ONLINEARRAY} 'for x in `vol --info '$vol' | grep "Initiator Group:" | cut -d : -f 2`; do vol --removeacl '$vol' --initiatorgrp $x; done'
    $SSHCMD admin@${ONLINEARRAY} 'vol --addacl '$vol' --initiatorgrp '$INITGRP
done

#### Example clone for DR Test

#Clone from Array B for batch DB
echo "Create clone volumes for Batch DB after midnight"
$SSHCMD admin@${BATCHARRAY} 'volcoll --snap ${VOLCOLL} --snapcoll_name DR-Test'
VOLUMES=$(${SSHCMD} admin@${BATCHARRAY} "volcoll --info ${VOLCOLL}" | grep "Associated volumes:" | cut -d: -f2 | tr -d ',' | xargs -n 1 | sort)
for vol in $VOLUMES; do
  $SSHCMD admin@${BATCHARRAY} 'vol --clone '$vol' --clonename '$vol'-batch --snapname DR-Test'
  $SSHCMD admin@${BATCHARRAY} 'for x in `vol --info '$vol' | grep "Initiator Group:" | cut -d : -f 2`; do vol --removeacl '$vol' --initiatorgrp $x; done'
  $SSHCMD admin@${BATCHARRAY} 'for x in `vol --info '$vol'-batch | grep "Initiator Group:" | cut -d : -f 2`; do vol --removeacl '$vol'-batch --initiatorgrp $x; done'
  $SSHCMD admin@${BATCHARRAY} 'vol --addacl '$vol'-batch --initiatorgrp '$INITGRP
done