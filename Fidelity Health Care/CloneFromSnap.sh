VOLCOLL="SQL"
INITGRP="SQLServer"

volcoll --snap ${VOLCOLL} --snapcoll_name DR-Test
VOLUMES=$(volcoll --info ${VOLCOLL} | grep "Associated volumes:" | cut -d: -f2 | tr -d ',' | xargs -n 1 | sort))
for vol in $VOLUMES; do
  vol --clone ${vol} --clonename ${vol}-DR --snapname DR-Test
  for x in `vol --info ${vol}-DR | grep "Initiator Group:" | cut -d : -f 2`; do
    vol --removeacl ${vol}-DR --initiatorgrp $x
  done
  vol --addacl ${vol}-DR --initiatorgrp ${INITGRP}
done