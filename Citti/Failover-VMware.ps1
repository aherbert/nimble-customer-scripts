## <copyright file="Set-VMwarePSP.ps1" company="NimbleStorage">
## Copyright (c) 2013 All Right Reserved, http://www.nimblestorage.com/
##
## THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
## KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
## PARTICULAR PURPOSE.
##
## </copyright>
## <author>Adam Herbert</author>
## <email>aherbert@nimblestorage.com</email>
## <date>2014-07-29</date>
## <summary>Promotes Volume Collections and mounts datastores for simple DR</summary>
## <references>
## 

param (
  [string]$vmhostName = "",
  [string]$vcSession = ""
)

#### Configuration 
#Nimble Info
$LocalNimbleIP = "10.18.128.50"
$RemoteNimbleName = "group-rtp-array22"
# Add public key to Nimble
# sshkey --add AutomateDR --type rsa --key <public_key>
$PlinkPath = "C:\plink.exe" 
$PlinkArgs = "-i c:\nimble.ppk"

#VMware VCenter Info
$VCenter = "10.18.128.25"
$VCenterUser = "Administrator"
$VCenterPassword = "Nim123Boli"
#### End Configuration

# Promote Volume Collection
# Be sure host key is accepted
$Cmd = "echo `"Y`" | " + $PlinkPath + " " + $PlinkArgs + " admin@" + $LocalNimbleIP + " exit"
Invoke-Expression $Cmd
$Cmd = $PlinkPath + " " + $PlinkArgs + " -batch admin@" + $LocalNimbleIP + `
  " `"for x in ```$(volcoll --list | grep " + $RemoteNimbleName + " | awk '{print ```$1}'); do volcoll --promote ```$x; done`""
Invoke-Expression $Cmd

if ((Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null) {
  try {
    Add-PSSnapin VMware.VimAutomation.Core -ErrorAction Stop
  }
  catch {
    Write-Host "Please install VMware PowerCLI"
    Return
  }
}

function rescan ($vmhostName) {
  $vmhost = Get-VMHost -Name $vmhostName
  Write-Host "Rescanning HBA on" $vmhost.Name
  Get-VMHostStorage -VMHost $vmhost -RescanAllHba -RescanVmfs | Out-Null
  
  Write-Host "Getting esxcli"
  $esxcli = Get-EsxCli -VMHost $vmhost
  
  Write-Host "Getting list of unresolved volumes"
  $unBoundVolumes = $esxcli.storage.vmfs.snapshot.list()
  if ($unBoundVolumes.Count -gt 0) {
    Write-Host -NoNewline "Found the following unresolved volumes"
    $unBoundVolumes | Format-Table "VolumeName", "UnresolvedExtentCount", "Canmount", "Reasonforunmountability"

    foreach ($volume in $unBoundVolumes) {
      if ($volume.Canmount -eq $true) {
        Write-Host "Attempting to mount" $volume.VolumeName "on" $vmhost.Name
        $return = $esxcli.storage.vmfs.snapshot.mount($false, $volume.VolumeName, $null)
        Write-Host "Mount result"
        $return | Format-List
      }
      else {
        Write-Host "Can't mount" $volume.VolumeName ":" $volume.Reasonforunmountability
      }
    }
  }
}

# Connect to VCMS and get list of hosts
try {
  if ($vcSession -ne "") {
    $vc = Connect-VIServer -Server $VCenter -Session $vcSession -WarningAction 0    
  }
  elseif ($VCenterUser -ne "") {
    $vc = Connect-VIServer -Server $VCenter -User $VCenterUser -Password $VCenterPassword -WarningAction 0
  }
  else {
    $vc = Connect-VIServer -Server $VCenter -WarningAction 0
  }
}
catch {
  Write-Host "Could not connect to vCenter (" $vcms_ip ") and get hosts" $?
  Return
}

if ($vmhostName -eq "") {
  # $vmhosts = Get-VMHost -ErrorAction Stop | ? { ($_.Name -like "98esx*") -and ($_.Name -notlike "*ucs*") }
  $vmhosts = Get-VMHost -ErrorAction Stop
  foreach ($vmhost in $vmhosts) {
    Write-Host "Starting job for" $vmhost.Name
    $jobs = Start-Job -PSVersion "2.0" -RunAs32 -ScriptBlock { `
      powershell -command "$args[0] -vmhostName $($args[1]) -vcSession $($args[2])" `
    } `
    -ArgumentList $MyInvocation.MyCommand.Definition, $vmhost.Name, $vc.SessionSecret
  }
  
  # Wait for running jobs
  while (@(Get-Job | Where { $_.State -eq "Running" }).Count -ne 0) {
    Write-Host "Waiting for background jobs..."
    Start-Sleep -Seconds 3
  }
  
  $results = ForEach ($job in (Get-Job)) {  
    Receive-Job $job
    Remove-Job $job
  }
  
  $results
}
else {
  rescan ($vmhostName)
}

# Logout of the VCMS
Disconnect-VIServer -Confirm:$false -ErrorAction SilentlyContinue
