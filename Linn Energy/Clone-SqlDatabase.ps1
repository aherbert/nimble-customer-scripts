<# <copyright file="Refresh-NimbleCloneLive.ps1" company="NimbleStorage">
## Copyright (c) 2017 All Right Reserved, http://www.nimblestorage.com/
##
## THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
## KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
## PARTICULAR PURPOSE.
##
## </copyright>
## <author>Adam Herbert</author>
## <email>aherbert@nimblestorage.com</email>
## <date>2017-02-15</date>
## <summary>Refresh SQL clones dynamically by layout on source 
## SQL server</summary>
##>

param(
  [Parameter(Mandatory=$true)]
  [string]$NimbleGroup,

  # Specify source server
  [Parameter(Mandatory=$true)]
  [string]$SourceServer,

  # Specify source DB
  [Parameter(Mandatory=$true)]
  [string[]]$SourceDB,

  # Specify target server
  [Parameter(Mandatory=$true)]
  [string]$TargetServer,

  # Specify target DB suffix
  [Parameter(Mandatory=$false)]
  [string]$TargetDBSuffix,

  # Specify target DB prefix
  [Parameter(Mandatory=$false)]
  [string]$TargetDBPrefix,

  # Specify target DB prefix
  [Parameter(Mandatory=$false)]
  [string]$TargetCloneRoot="C:\Nimble",

  # Specify post process SQL script
  [Parameter(Mandatory=$false)]
  [string]$SQLScript,

  # Switch to enable logging to console
  [Parameter(Mandatory=$false)]
  [switch]$LogConsole=$true,

  [Parameter(Mandatory=$false)]
  [switch]$LogEvent=$false,

  [Parameter(Mandatory=$false)]
  [switch]$LogEmail=$false,

  [Parameter(Mandatory=$false)]
  [string]$smtpServer,

  [Parameter(Mandatory=$false)]
  [string]$emailTo,

  [Parameter(Mandatory=$false)]
  [string]$emailFrom,

  [Parameter(Mandatory=$false)]
  [string]$emailSubject,

  [Parameter(Mandatory=$false)]
  [switch]$NoCreate=$false,

  [Parameter(Mandatory=$false)]
  [switch]$ResetNimblePassword=$false
)

# Start timer for code execution
$stopWatch = [System.Diagnostics.Stopwatch]::StartNew()

# Set default email subject
if ($emailSubject -eq "") {
  $scriptName = & { $MyInvocation.ScriptName } | Split-Path -Leaf
  $emailSubject = "Output of $scriptName"
}

# Disable SSL checking for communication to the array
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

# Function for handling log output
Function Write-Log {
  Param(   
    [Parameter(Mandatory=$true,
    ValueFromPipelineByPropertyName=$true)]
    [string]$LogMessage,
    
    [Parameter(Mandatory=$false,
    ValueFromPipelineByPropertyName=$true)]
    [ValidateSet('Error', 'Information', 'FailureAudit', 'SuccessAudit', 'Warning')]
    [string]$LogLevel="Information"
  )
  if ($stopWatch -ne $null -and $stopWatch.GetType().Name -eq "Stopwatch") {
    $executionTime = $stopWatch.Elapsed.TotalSeconds
    $LogMessage = [string]::Format("{0,6:N2}s: {1}", $executionTime, $LogMessage)
  }
  $scriptName = & { $MyInvocation.ScriptName } | Split-Path -Leaf
  if ($LogEmail) {
      $Script:emailBody += "$LogLevel : $LogMessage`n"
  }
  if ($LogEvent) {
    # Create Event Log Source
    New-EventLog -LogName Application -Source $scriptName -ErrorAction SilentlyContinue
    Write-EventLog -LogName Application -EventId "1" -Source $scriptName -EntryType $LogLevel -Message "$LogMessage"
  }
  if ($LogConsole) {
    if ($LogLevel -ieq "Error" -or $LogLevel -ieq "FailureAudit") {
      Microsoft.PowerShell.Utility\Write-Host -ForegroundColor Red $LogMessage
    }
    elseif ($LogMessage -ieq "Warning") {
      Microsoft.PowerShell.Utility\Write-Host -ForegroundColor Yellow $LogMessage
    }
    else {
      Microsoft.PowerShell.Utility\Write-Host $LogMessage
    }
  }
}

# Function to send log email
Function Send-LogEmail {
  param(
    [Parameter(Mandatory=$true,
    ValueFromPipelineByPropertyName=$true)]
    [string]$smtpServer,
    [Parameter(Mandatory=$true,
    ValueFromPipelineByPropertyName=$true)]
    [string]$emailTo,
    [Parameter(Mandatory=$true,
    ValueFromPipelineByPropertyName=$true)]
    [string]$emailFrom,
    [Parameter(Mandatory=$false,
    ValueFromPipelineByPropertyName=$true)]
    [string]$emailSubject,
    [Parameter(Mandatory=$true,
    ValueFromPipelineByPropertyName=$true)]
    [string]$emailBody
  )
  if ($emailSubject -eq "") {
    $scriptName = & { $MyInvocation.ScriptName } | Split-Path -Leaf
    $emailSubject = "Output of $scriptName"
  }
  try{ 
    Send-MailMessage -SmtpServer $smtpServer -From $emailFrom -To $emailTo -Subject $emailSubject -Body $emailBody
  }
  catch {
    Write-Log -LogLevel Error -LogMessage "Couldn't send email!"
    Write-Error -LogLevel Error -LogMessage $_
  }
}

# Handle exceptions and log them
trap {
  $LogMessage = "Exception: $($_.Exception.Message) at line:$($_.InvocationInfo.ScriptLineNumber) char:$($_.InvocationInfo.OffsetInLine)" 
  Write-Log -LogLevel FailureAudit -LogMessage $logMessage
  if ($LogEmail) {
    $emailSubject = "FAILED! - $($emailSubject)"
    Send-LogEmail -smtpServer $smtpServer -emailTo $emailTo -emailFrom $emailFrom -emailSubject $emailSubject -emailBody $Script:emailBody
  }
  Exit
}

# Enable stoping on errors now that trap handling code is in place
$ErrorActionPreference = "Stop"

Import-Module "sqlps" -DisableNameChecking

# Load System.Security assembly 
$result = [Reflection.Assembly]::LoadWithPartialName("System.Security") 

Function Get-StringHash
{
   param(
    [Parameter(Mandatory=$true,
    ValueFromPipelineByPropertyName=$true)]
    [string]$String,
    [Parameter(Mandatory=$false,
    ValueFromPipelineByPropertyName=$true)]
    [string]$HashName="MD5"
  )
  $StringBuilder = New-Object System.Text.StringBuilder
  [System.Security.Cryptography.HashAlgorithm]::Create($HashName).ComputeHash([System.Text.Encoding]::UTF8.GetBytes($String)) | % {
    [Void]$StringBuilder.Append($_.ToString("x2"))
  }
  $StringBuilder.ToString()
}


function Encrypt-String($String, $Passphrase, $salt="My Voice is my P455W0RD!", $init="Yet another key", [switch]$arrayOutput)
{
   $r = new-Object System.Security.Cryptography.RijndaelManaged
   $pass = [Text.Encoding]::UTF8.GetBytes($Passphrase)
   $salt = [Text.Encoding]::UTF8.GetBytes($salt)

   $r.Key = (new-Object Security.Cryptography.PasswordDeriveBytes $pass, $salt, "SHA1", 5).GetBytes(32) #256/8
   $r.IV = (new-Object Security.Cryptography.SHA1Managed).ComputeHash( [Text.Encoding]::UTF8.GetBytes($init) )[0..15]
   
   $c = $r.CreateEncryptor()
   $ms = new-Object IO.MemoryStream
   $cs = new-Object Security.Cryptography.CryptoStream $ms,$c,"Write"
   $sw = new-Object IO.StreamWriter $cs
   $sw.Write($String)
   $sw.Close()
   $cs.Close()
   $ms.Close()
   $r.Clear()
   [byte[]]$result = $ms.ToArray()
   if($arrayOutput) {
      return $result
   } else {
      return [Convert]::ToBase64String($result)
   }
}

function Decrypt-String($Encrypted, $Passphrase, $salt="My Voice is my P455W0RD!", $init="Yet another key")
{
   if($Encrypted -is [string]){
      $Encrypted = [Convert]::FromBase64String($Encrypted)
   }

   $r = new-Object System.Security.Cryptography.RijndaelManaged
   $pass = [System.Text.Encoding]::UTF8.GetBytes($Passphrase)
   $salt = [System.Text.Encoding]::UTF8.GetBytes($salt)

   $r.Key = (new-Object Security.Cryptography.PasswordDeriveBytes $pass, $salt, "SHA1", 5).GetBytes(32) #256/8
   $r.IV = (new-Object Security.Cryptography.SHA1Managed).ComputeHash( [Text.Encoding]::UTF8.GetBytes($init) )[0..15]

   $d = $r.CreateDecryptor()
   $ms = new-Object IO.MemoryStream @(,$Encrypted)
   $cs = new-Object Security.Cryptography.CryptoStream $ms,$d,"Read"
   $sr = new-Object IO.StreamReader $cs
   Write-Output $sr.ReadToEnd()
   $sr.Close()
   $cs.Close()
   $ms.Close()
   $r.Clear()
}

Function Connect-NSGroup {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipelineByPropertyName=$true)]
    [string]$NimbleGroup=$Script:NimbleGroup
  )

  if ($Global:session_token) {
    try {
      $result = Send-NSRequest -RequestMethod get -RequestTarget "tokens/detail" -RequestArguments @{ "session_token" = $Global:session_token }
      return $result.data
    }
    catch {
      $Global:session_token = $null
    }
  }

  <# Get Nimble credential from registry #>
  if (!(Test-Path "HKCU:\Software\NimbleStorage\Credentials\$NimbleGroup")) { 
    $result = New-Item -Path "HKCU:\Software\NimbleStorage\Credentials" -Name $NimbleGroup -Force
  }
  else {
    $storedCredential = Get-Item -Path "HKCU:\Software\NimbleStorage\Credentials\$NimbleGroup"
  }
  <##>

  <# Create credential object #>
  if ($storedCredential.getValue("Username") -and $storedCredential.getValue("Password") -and !($ResetPassword)) {
    $nimbleUsername = (Get-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Username").Username
    $passphrase = Get-StringHash -String $($nimbleUsername + $NimbleGroup)
    $nimbleEncryptedPassword = (Get-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Password").Password
    $nimblePassword = Decrypt-String -Encrypted $nimbleEncryptedPassword -Passphrase $passphrase -salt $nimbleUsername -init $NimbleGroup | ConvertTo-SecureString -AsPlainText -Force
    $nimbleCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $nimbleUsername, $nimblePassword
  }
  else {
    $nimbleCredential = Get-Credential -Message "Enter username and password for $NimbleGroup"
    try {
      Set-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Username" -Value $nimbleCredential.UserName
      $passphrase = Get-StringHash -String $($nimbleCredential.UserName + $NimbleGroup)
      $nimbleEncryptedPassword = Encrypt-String -String ($nimbleCredential.GetNetworkCredential().Password) -Passphrase $passphrase -salt $nimbleUsername -init $NimbleGroup
      Set-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Password" -Value $nimbleEncryptedPassword
    }
    catch {
      Write-Log -LogLevel Warning -LogMessage "Couldn't save password"
    }
  }
  <##>

  $scriptName = & { $MyInvocation.ScriptName } | Split-Path -Leaf
  $networkCredential = $nimbleCredential.GetNetworkCredential()
  $result = Send-NSRequest -RequestMethod Post -RequestTarget "tokens" -RequestArguments @{ 
    username = $networkCredential.UserName
    password = $networkCredential.Password
    app_name = $scriptName
  }
  $Global:session_token = $result.data.session_token
  return $result.data
}

Function Send-NSRequest {
<#
.SYNOPSIS
Abstract the Invoke-Restmethod for all other module cmdlets

.DESCRIPTION
.PARAMETER RequestMethod
  Set the request method used. Valid options are 'get', 'delete', 'post', 'put'
.PARAMETER RequestTarget
  Set the request object and any sub object
.PARAMETER RequestArguments
  Set URI arguments for GET requests
.PARAMETER RequestHeaders
  Set any additional headers. The X-Auth-Token header is set automatically from the $global:session_token variable
.PARAMETER RequestBody
  Set the Request Body. 
.INPUTS
.OUTPUTS
.EXAMPLE
.EXAMPLE
.LINK
#>
  [CmdletBinding()]
  Param
  (
    [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
    [ValidateSet('get', 'delete', 'post', 'put')]
    [string]$RequestMethod,

    [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
    [string]$RequestTarget,
        
    [Parameter(Mandatory=$false,ValueFromPipelineByPropertyName=$true)]
    [hashtable]$RequestArguments,

    [Parameter(Mandatory=$false,ValueFromPipelineByPropertyName=$true)]
    [hashtable]$RequestHeaders = @{},
        
    [Parameter(Mandatory=$false,ValueFromPipelineByPropertyName=$true)]
    [string]$NimbleGroup=$Script:NimbleGroup
  )

  if ( ![string]::IsNullOrEmpty($Global:session_token) ) { $RequestHeaders.Add("X-Auth-Token", $Global:session_token) }

  $uri = 'https://' + $NimbleGroup + ':5392/'
  if ($RequestTarget -eq 'versions') {
    $uri = $uri + 'versions'
  }
  else {
    $uri = $uri + 'v1/' + $RequestTarget
  }

  if ( $RequestMethod -ieq 'get' ) {
    if ($RequestArguments.Count -gt 0) {
      $uri += '?'
      $uri += [string]::join("&", @(foreach($pair in $RequestArguments.GetEnumerator()) { 
        if ($pair.Name) { 
          $pair.Name + '=' + $pair.Value 
        } 
      }))
    }
  }
  else {
    $RequestJSON = @{ data = $RequestArguments } | ConvertTo-Json -Depth 5
  }

  try {
    $request = [System.Net.WebRequest]::Create($uri)
    $request.Method = $RequestMethod.ToUpper()
    $request.ContentType = "application/json"
    $RequestHeaders.GetEnumerator() | % { $request.Headers.Add($_.Key, $_.Value) }
    if ( $RequestMethod -ine 'GET' -and $RequestArguments.Count -gt 0 ) {
      $requestStream = $request.GetRequestStream();
      $streamWriter = New-Object System.IO.StreamWriter $requestStream
      $streamWriter.Write($RequestJSON.ToString())
      $streamWriter.Flush()
      $streamWriter.Close()
    }
 
    $response = $request.GetResponse()
  }
  catch {
    $httpStatus = [regex]::matches($_.exception.message, "(?<=\()[\d]{3}").Value
    if ( $httpStatus -eq '401' ) {
      Throw 'You must login into the Nimble Array first'
    }
    elseif ($httpStatus -ne $null) {
      $responseStream = $_.Exception.InnerException.Response.GetResponseStream()
      $readStream = New-Object System.IO.StreamReader $responseStream
      $data = $readStream.ReadToEnd()
      $results = $data | ConvertFrom-Json
      Throw "$('There was an error, status code: ' + $httpStatus)`n$($uri)`n$($RequestJSON.ToString())`n$($results.messages | Format-Table | Out-String)"
    }
    else { 
      Throw $_.Exception
    }
    Return
  }
  $responseStream = $response.GetResponseStream()
  $readStream = New-Object System.IO.StreamReader $responseStream
  $data = $readStream.ReadToEnd()
  $results = New-Object -TypeName PSCustomObject
  $results | Add-Member -MemberType NoteProperty -Name StatusCode -Value "$([int]$response.StatusCode) - $($response.StatusCode)" 
  $results | Add-Member -MemberType NoteProperty -Name data -Value $($data | ConvertFrom-Json).data

  Return $results
}

Function Get-DiskDetail {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipelineByPropertyName=$true)]
    [string]$ComputerName=$env:COMPUTERNAME
  )
  $disks = Invoke-Command -ComputerName $ComputerName -ScriptBlock { Get-Disk }
  $partitions = Invoke-Command -ComputerName $ComputerName -ScriptBlock { Get-Partition }
  $results = @()
  foreach($disk in $disks) {
    foreach ($partition in $partitions | ? { $_.DiskNumber -eq $disk.Number }) {
      foreach ($accessPath in $partition.AccessPaths | ? { $_ -and $_ -notmatch '\\\\?\\' -and $_ -notmatch '\$Recycle.Bin'}) {
        $resultObject = New-Object PSObject -Property @{
          DiskNumber = $disk.Number
          DiskSerial = $disk.SerialNumber
          DiskFriendlyName = $disk.FriendlyName
          DiskSize = $disk.Size
          PartitionStyle = $disk.PartitionStyle
          PartitionNumber = $partition.PartitionNumber
          PartitionSize = $partition.Size
          AccessPath = $accessPath
        }
        $results += $resultObject
      }
    }
  }
  return $results
}

Function Get-NSSnapCollection {
  param(
    [Parameter(Mandatory=$false)]
    [string]$VolumeCollection,
    [Parameter(Mandatory=$false)]
    [string]$SnapCollectionId,
    [Parameter(Mandatory=$false)]
    [hashtable]$RequestArgument=@{}
  )
  if ($SnapCollectionId) {
    $requestTarget = "snapshot_collections/$SnapCollectionId"
  }
  else {
    $requestTarget = "snapshot_collections/detail"
    if ($VolumeCollection) { $RequestArgument.volcoll_name = $VolumeCollection }
  }
  (Send-NSRequest -RequestMethod get -RequestTarget $requestTarget -RequestArguments $RequestArgument).data


}

Function Clone-NSSnapshot {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipeline=$true)]
    [System.Object[]]$SnapshotObject,
    [Parameter(Mandatory=$false)]
    [string]$SnapshotId,
    [Parameter(Mandatory=$false)]
    [string]$CloneName,
    [Parameter(Mandatory=$false)]
    [string]$CloneSuffix,
    [Parameter(Mandatory=$false)]
    [string]$ClonePrefix,
    [Parameter(Mandatory=$false)]
    [string]$Description,
    [Parameter(Mandatory=$false)]
    [hashtable]$Metadata
  )

  Process {
    if ($SnapshotObject.snap_id -eq "") {
      $SnapshotObject = Get-Snapshot -SnapshotId = $SnapshotId
    }
    if ($ClonePrefix -or $CloneSuffix) {
      $CloneName = $ClonePrefix + $SnapshotObject.vol_name + $CloneSuffix
    }
    elseif (!$CloneName) {
      $CloneName = $SnapshotObject.vol_name + "-Clone"
    }
    $request = @{
      name = $CloneName
      clone = $true
      online = $false
      base_snap_id = $SnapshotObject.snap_id
    }
    if ($Description) { $request.Add("description", $Description) }
    foreach ($key in $Metadata.Keys) {
      $request.metadata += @(@{"key" = $key; "value" = $($Metadata.Item($key))})
    }

    $result = Send-NSRequest -RequestMethod post -RequestTarget "volumes" -RequestArguments $request
    $result.data
  }
}

Function Get-SqlDatabasePath {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipelineByPropertyName=$true)]
    [string]$ServerInstance="$($env:COMPUTERNAME)",
    [Parameter(Mandatory=$true,
    ValueFromPipelineByPropertyName=$true)]
    [string]$Database
  )
  $SQL = @"
SELECT
    db.name AS DBName,
    type_desc AS FileType,
    Physical_Name AS Location
FROM
    sys.master_files mf
INNER JOIN 
    sys.databases db ON db.database_id = mf.database_id
WHERE
    db.name = '$database'
"@
  Invoke-Sqlcmd -ServerInstance $ServerInstance -Query $SQL
}

Function Get-SqlDbDisk {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipelineByPropertyName=$true)]
    [string]$ServerInstance="$($env:COMPUTERNAME)",
    [Parameter(Mandatory=$true,
    ValueFromPipelineByPropertyName=$true)]
    [string]$Database
  )
  $ComputerName = ($ServerInstance.Split("\"))[0]
  $dbPathList = Get-SQLDatabasePath -ServerInstance $ServerInstance -Database $Database
  $volumeDetail = Get-DiskDetail -ComputerName $ComputerName

  $results = @()
  foreach ($dbPath in $dbPathList) {
    $SqlDisk = $volumeDetail | ? { $dbPath.Location -ilike "$($_.AccessPath)*" } |
      Sort-Object -Descending { $_.AccessPath.length } |
      Select-Object -First 1
    foreach ($Property in $($SqlDisk | Get-Member -MemberType NoteProperty, Properties)) {
      $dbPath | Add-Member -MemberType $Property.MemberType -Name $Property.Name `
      -Value $SqlDisk.$($Property.Name) -ErrorAction SilentlyContinue
    }
    $dbPath | Add-Member -MemberType NoteProperty -Name "FilePath" -Value ($dbPath.Location).Substring($dbPath.AccessPath.Length)
    $results += $dbPath
  }
  return $results
}

Function Get-NimbleVolumeForSqlDb {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipelineByPropertyName=$true)]
    [string]$ServerInstance="$($env:COMPUTERNAME)",
    [Parameter(Mandatory=$true,
    ValueFromPipelineByPropertyName=$true)]
    [string]$Database
  )

  $sqlDiskList = Get-SqlDbDisk -ServerInstance $SourceServer -Database $database

  $results = @()
  foreach ($sqlDisk in $sqlDiskList) {
     $nimbleVolume = Get-NSVolume -RequestArgument @{ "serial_number" = $sqlDisk.DiskSerial }
     $sqlDisk | Add-Member -MemberType NoteProperty -Name "NimbleVolume" -Value $nimbleVolume
     $results += $sqlDisk
  }
  return $results
}

Function Get-NSVolume {
  param(
    [Parameter(Mandatory=$false)]
    [string]$VolumeID,
    [Parameter(Mandatory=$false)]
    [string]$Volume,
    [Parameter(Mandatory=$false)]
    [hashtable]$RequestArgument=@{}
  )

  if ($VolumeID) {
    $requestTarget = "volumes/$VolumeID"
  }
  else {
    $requestTarget = "volumes/detail"
    if ($Volume -ne "") { $RequestArgument.name = $Volume }
  }
  (Send-NSRequest -NimbleGroup $NimbleGroup -RequestMethod get -RequestTarget $requestTarget -RequestArguments $RequestArgument).data
}

Function Set-NSVolume {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipeline=$true)]
    [System.Object[]]$VolumeObject,
    [Parameter(Mandatory=$false)]
    [string]$VolumeId,
    [Parameter(Mandatory=$false)]
    [switch]$Online,
    [Parameter(Mandatory=$false)]
    [switch]$Force,
    [Parameter(Mandatory=$false)]
    [hashtable]$RequestArgument=@{}
  )

  Process {
    if ($VolumeObject.id -eq "") {
      $VolumeObject = Get-NSVolume -VolumeId $VolumeId
    }

    $RequestArgument.online = $Online.ToString().ToLower()
    $RequestArgument.force = $Force.ToString().ToLower()

    $result = Send-NSRequest -RequestMethod put -RequestTarget $("volumes/" + $VolumeObject.id) -RequestArguments $RequestArgument
    $result.data
  }
}

Function Remove-NSVolume {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipeline=$true)]
    [System.Object[]]$VolumeObject,
    [Parameter(Mandatory=$false)]
    [string]$VolumeId
  )

  Process {
    if ($VolumeObject.id -eq "") {
      $VolumeObject = Get-NSVolume -VolumeId $VolumeId
    }

    $result = Send-NSRequest -RequestMethod delete -RequestTarget $("volumes/" + $VolumeObject.id)
    $result.data
  }
}

Function Set-NSVolumeInitiatorGroup {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipeline=$true)]
    [System.Object[]]$VolumeObject,
    [Parameter(Mandatory=$false)]
    [string]$VolumeId,
    [Parameter(Mandatory=$false)]
    [string]$InitiatorGroupName
  )

  Process {
    if ($VolumeObject.id -eq "") {
      $VolumeObject = Get-NSVolume -VolumeId $VolumeId
    }

    $initiatorGroupObject = Get-NSInitiatorGroup -InitiatorGroup $InitiatorGroupName

    foreach ($acl in $VolumeObject.access_control_records) {
      $result = Send-NSRequest -RequestMethod delete -RequestTarget $("access_control_records/" + $acl.id)
    }

    
    $RequestArgument = @{
      "vol_id" = $VolumeObject.id
      "initiator_group_id" = $initiatorGroupObject.id
    }

    $result = Send-NSRequest -RequestMethod post -RequestTarget "access_control_records/" -RequestArguments $RequestArgument
  }
}

Function Get-NSSnapshot {
  param(
    [Parameter(Mandatory=$false)]
    [string]$SnapshotID,
    [Parameter(Mandatory=$false)]
    [string]$Snapshot,
    [Parameter(Mandatory=$false)]
    [hashtable]$RequestArgument=@{}
  )
  if ($SnapshotID) {
    $requestTarget = "snapshots/$SnapshotID"
  }
  else {
    $requestTarget = "snapshots/detail"
    if ($Snapshot) { $requestArgument.name = $Snapshot }
  }
  (Send-NSRequest -NimbleGroup $NimbleGroup -RequestMethod get -RequestTarget $requestTarget -RequestArguments $requestArgument).data
}

Function Get-NSInitiator {
  param(
    [Parameter(Mandatory=$false)]
    [string]$Initiator,
    [Parameter(Mandatory=$false)]
    [string]$InitiatorID,
    [Parameter(Mandatory=$false)]
    [hashtable]$RequestArgument=@{}
  )
  if ($InitiatorID) {
    $requestTarget = "initiators/$InitiatorID"
  }
  else {
    $requestTarget = "initiators/detail"
    if ($Initiator) { $requestArgument.name = $Initiator }
  }
  (Send-NSRequest -NimbleGroup $NimbleGroup -RequestMethod get -RequestTarget $requestTarget -RequestArguments $requestArgument).data
}

Function Get-NSInitiatorGroup {
  param(
    [Parameter(Mandatory=$false)]
    [string]$InitiatorGroup,
    [Parameter(Mandatory=$false)]
    [string]$InitiatorGroupID,
    [Parameter(Mandatory=$false)]
    [hashtable]$RequestArgument=@{}
  )
  if ($InitiatorGroupID) {
    $requestTarget = "initiator_groups/$InitiatorGroupID"
  }
  else {
    $requestTarget = "initiator_groups/detail"
    if ($InitiatorGroup) { $requestArgument.name = $InitiatorGroup }
  }
  (Send-NSRequest -NimbleGroup $NimbleGroup -RequestMethod get -RequestTarget $requestTarget -RequestArguments $requestArgument).data
}

Function Select-InitiatorGroupByInitiator {
  param(
    [Parameter(Mandatory=$true,
    ValueFromPipeline=$true)]
    [System.Object[]]$InitiatorGroupList,
    [Parameter(Mandatory=$true)]
    [string[]]$Initiator
  )

  Process {
    foreach ($initiatorGroup in $InitiatorGroupList) {
      foreach ($iscsiInitiator in $initiatorGroup.iscsi_initiators) {
        if ($initiator -icontains $iscsiInitiator.iqn -or $initiator -contains $iscsiInitiator.initiator_id) {
          $initiatorGroup
        } 
      }
      foreach ($fcInitiator in $initiatorGroup.fc_initiators) {
        if ($initiator -icontains $fcInitiator.wwpn -or $initiator -contains $iscsiInitiator.initiator_id) {
          $initiatorGroup
        } 
      }
    }
  }
}

Function Get-HostInitiatorName {
  param(
    [Parameter(Mandatory=$false)]
    [string]$ComputerName="$($env:COMPUTERNAME)"
  )

  Process {
    <# Get FC WWPNs #>
    try {
      $fcHbaLists = Get-WmiObject @{
        Namespace    = 'root\WMI'
	    class        = 'MSFC_FCAdapterHBAAttributes'
	    ComputerName = $ComputerName 
	    ErrorAction  = 'Stop'
      }
      foreach ($fcHba in $fcHbas) {
        ($fcHba.NodeWWN | % { "{0:X2}" -f $_ } ) -join ":"
      }
    }
    catch {}
    <# Get iSCSI IQN #>
    Invoke-Command -ComputerName $ComputerName -ScriptBlock {
      Get-InitiatorPort | Select -ExpandProperty NodeAddress
    }
  }
}

Function Connect-IscsiIqn {
  param(
    [Parameter(Mandatory=$true,
    ValueFromPipeline=$true)]
    [string[]]$IQN,
    [Parameter(Mandatory=$false)]
    [string]$ComputerName="$($env:COMPUTERNAME)"
  )

  Begin {
    $result = (Get-WmiObject -ComputerName $ComputerName -Namespace root/wmi MSiSCSIInitiator_MethodClass).RefreshTargetList()
    #$iscsiTargetList = Get-WmiObject -ComputerName $ComputerName -Namespace root/wmi MSIscsiInitiator_TargetClass
  }
  Process {
    Invoke-Command -ComputerName $ComputerName -ScriptBlock {
      $result = Connect-IscsiTarget -NodeAddress $Using:IQN -IsPersistent $true -IsMultipathEnabled $true
    }
  }
}

Function Disconnect-IscsiIqn {
  param(
    [Parameter(Mandatory=$true,
    ValueFromPipeline=$true)]
    [string[]]$IQN,
    [Parameter(Mandatory=$false)]
    [string]$ComputerName="$($env:COMPUTERNAME)"
  )

  Process {
    Invoke-Command -ComputerName $ComputerName -ScriptBlock {
      $IQN = $Using:IQN
      $iscsiSessionList = Get-WmiObject -Namespace root/wmi MSIscsiInitiator_SessionClass | ? { $_.TargetName -eq $IQN }
      $iscsiSessionList | % { $result = $_.Logout() }
      $persistentLoginList = Get-WmiObject -Namespace root/wmi MSIscsiInitiator_PersistentLoginClass | ? { $_.TargetName -eq $IQN }
      foreach ($persistentLogin in $persistentLoginList) {
        $result = iscsicli RemovePersistentTarget $persistentLogin.InitiatorInstance $IQN $persistentLogin.InitiatorPortNumber $persistentLogin.TargetPortal.Address $persistentLogin.TargetPortal.Port
      }
    }
  }
}

Function Mount-NimbleClone {
  param(
    [Parameter(Mandatory=$true,
    ValueFromPipeline=$true)]
    [System.Object[]]$NimbleVolume,
    [Parameter(Mandatory=$false)]
    [string]$ComputerName="$($env:COMPUTERNAME)"
  )

  Begin {
    Invoke-Command -ComputerName $ComputerName -ScriptBlock { "rescan `n exit" | diskpart | Out-Null }
  }
  Process {
    $return = Invoke-Command -ComputerName $ComputerName -ScriptBlock {
      $NimbleVolume = $Using:NimbleVolume
      $CloneRoot = $Using:TargetCloneRoot
      $disk = Get-Disk -UniqueId $NimbleVolume.serial_number
      Set-Disk -Number $disk.Number -IsReadOnly $false
      Set-Disk -Number $disk.Number -IsOffline $false
      $partitionList = Get-Partition -DiskNumber $disk.Number

      foreach ($partition in $partitionList) {
        $volume = Get-Volume -Partition $partition
        if ($volume -eq $null) {
          Continue
        }
        Set-Partition -DiskNumber $disk.Number -PartitionNumber $partition.PartitionNumber -IsReadOnly $false -IsHidden $false -NoDefaultDriveLetter $true
        $partition = Get-Partition -DiskNumber $disk.Number -PartitionNumber $partition.PartitionNumber
        $volumePath = "\" + $NimbleVolume.name + "\" + $partition.PartitionNumber
        mkdir $($CloneRoot + $volumePath) -ErrorAction SilentlyContinue | Out-Null
        foreach ($accessPath in $partition.AccessPaths | ? { $_ -notmatch "Recycle" -and $_ -notmatch "\\\\?\\" }) {
          Remove-PartitionAccessPath -DiskNumber $disk.Number -PartitionNumber $partition.PartitionNumber -AccessPath $accessPath
        }
        Add-PartitionAccessPath -DiskNumber $disk.Number -PartitionNumber $partition.PartitionNumber -AccessPath $($CloneRoot + $volumePath)
        New-Object PSObject -Property @{
          DiskSerial = $disk.SerialNumber
          DiskNumber = $disk.Number
          PartitionNumber = $partition.PartitionNumber
          AccessPath = $($CloneRoot + $volumePath)
          NimbleVolume = $NimbleVolume
          Disk = $disk
          Partition = $partition
        } 
      }
    }
    $return
  }
}

Function Dismount-NimbleClone {
  param(
    [Parameter(Mandatory=$true,
    ValueFromPipeline=$true)]
    [System.Object[]]$NimbleVolume,
    [Parameter(Mandatory=$false)]
    [string]$ComputerName="$($env:COMPUTERNAME)"
  )

  Process {
    $return = Invoke-Command -ComputerName $ComputerName -ScriptBlock {
      $NimbleVolume = $Using:NimbleVolume
      $CloneRoot = $Using:TargetCloneRoot
      $disk = Get-Disk -UniqueId $NimbleVolume.serial_number
      $partitionList = Get-Partition -DiskNumber $disk.Number
      foreach ($partition in $partitionList) {
        foreach ($accessPath in $partition.AccessPaths | ? { $_ -notmatch "Recycle" -and $_ -notmatch "\\\\?\\" }) {
          if ($accessPath) {
            Remove-PartitionAccessPath -DiskNumber $disk.Number -PartitionNumber $partition.PartitionNumber -AccessPath $accessPath
          }
        }
      }
      Set-Disk -Number $disk.Number -IsReadOnly $true
      Set-Disk -Number $disk.Number -IsOffline $true
      Remove-Item -Path $($CloneRoot + "\" + $NimbleVolume.name) -Recurse -ErrorAction SilentlyContinue
    }
  }
}

Function Add-DatabaseClone {
  Param (
    [Parameter(Mandatory=$true)]
    [string]$ServerInstance,
    [Parameter(Mandatory=$true)]
    [string]$Database,
    [Parameter(Mandatory=$true)]
    [string[]]$FileList
  )

  Process {

    $sqlCmd = @"
USE [master]
GO
CREATE DATABASE [$Database] ON 
(FILENAME = '
"@
    $sqlCmd += $FileList -join "'),(FILENAME = '"
    $sqlCmd += @"
') for ATTACH
GO
"@

    Invoke-Sqlcmd -ServerInstance $ServerInstance -Query $sqlCmd -ErrorAction Continue
  }
}

Function Remove-DatabaseClone {
  Param (
    [Parameter(Mandatory=$true)]
    [string]$ServerInstance,
    [Parameter(Mandatory=$true)]
    [string]$Database
  )

  Process {
    $sqlCmd = 
@"
  USE [master]
  GO
  ALTER DATABASE [$Database] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE
  GO
  USE [master]
  GO
  EXEC master.dbo.sp_detach_db @dbname = N'$Database'
  GO
"@
    Invoke-Sqlcmd -ServerInstance $ServerInstance -Query $sqlCmd -ErrorAction Continue
  }
}

<# END FUNCTIONS #>

<# BEGIN Main Code #>

Write-Log -LogLevel Information -LogMessage "Connecting to $NimbleGroup"
$nimble = Connect-NSGroup

$ComputerName = ($TargetServer.Split("\"))[0]
$InstanceName = ($TargetServer.Split("\"))[1]
if ($InstanceName) { 
  $ClonePrefix = $ComputerName + "-" + $InstanceName + "-"
}
else {
  $ClonePrefix = $ComputerName + "-"
}

foreach ($database in $SourceDB) {
  $TargetDatabase = $TargetDBPrefix + $database + $TargetDBSuffix
  Write-Log -LogLevel Information -LogMessage "Source instance $SourceServer database $database cloning to $TargetServer as $TargetDatabase"

  $sqlVolumeList = Get-NimbleVolumeForSqlDb -ServerInstance $SourceServer -Database $database
  <# Validate that all volumes for a database are in a common VolColl #>
  $volumeCollection = ""
  $nimbleVolumeList = @()
  foreach ($sqlVolume in $sqlVolumeList) {
    if (-not $sqlVolume.NimbleVolume) {
      Throw "Volume for database $database is not a Nimble volume"
    }
    if ($volumeCollection -eq "") {
      $volumeCollection = $sqlVolume.NimbleVolume.volcoll_name
    }
    elseif ($volumeCollection -ne $sqlVolume.NimbleVolume.volcoll_name) {
      Throw "Volumes for $database are not in a common volume collection"
    }
    $nimbleVolumeList += $sqlVolume.NimbleVolume.name
    Write-Log -LogLevel Information -LogMessage ("Found Nimble volume " + $sqlVolume.NimbleVolume.name + " for source access path " + $sqlVolume.AccessPath)
  }
  <##>

  <# Validate target server and get initiator group #>
  $targetInitiatorList = Get-HostInitiatorName -ComputerName $ComputerName
  $targetInitiatorGroup = Get-NSInitiatorGroup | Select-InitiatorGroupByInitiator -Initiator $targetInitiatorList
  if ($targetInitiatorGroup.count -eq 0) {
    Throw "Couldn't find initiator group for $ComputerName"
  }
  elseif ($targetInitiatorGroup.count > 1) {
    $initiatorGroups = ($targetInitiatorGroup | Select -ExpandProperty name) -join ", "
    Throw "Target server in more that one initiator group: $initiatorGroups"
  }
  Write-Log -LogLevel Information -LogMessage ("Target host using initiator group " + $targetInitiatorGroup.name)
  <##>

  <# Remove Old Clones for TargetServer and TargetDatabase #>
  $oldClones = Get-NSVolume -RequestArgument @{ "clone" = "true"; "metadata.targetServer" = $TargetServer; "metadata.targetDatabase" = $TargetDatabase }
  if ($oldClones) {
    Write-Log -LogLevel Information -LogMessage ("Found old clones to remove: " + (($oldClones | Select -ExpandProperty Name) -join ', '))

    Write-Log -LogLevel Information -LogMessage "Detaching $database from $TargetServer"
    Remove-DatabaseClone -ServerInstance $TargetServer -Database $TargetDatabase

    Write-Log -LogLevel Information -LogMessage "Dismounting old clones"
    $oldClones | Dismount-NimbleClone -ComputerName $ComputerName

    Write-Log -LogLevel Information -LogMessage "Disconnecting iSCSI sessions"
    $oldClones | Select -ExpandProperty target_name | Disconnect-iScsiIqn -ComputerName $ComputerName

    Write-Log -LogLevel Information -LogMessage "Set old clones offline"
    $result = $oldClones | Set-NSVolume -Online:$false -Force

    Write-Log -LogLevel Information -LogMessage "Deleting old clones"
    $result = $oldClones | Remove-NSVolume 
  }

  if ($NoCreate) { continue }

  $dbSnapCollection = Get-NSSnapCollection -VolumeCollection $volumeCollection | Sort creation_time | Select -Last 1
  Write-Log -LogLevel Information -LogMessage ("Using snapshotCollection: " + $dbSnapCollection.name)

  $newClones = $dbSnapCollection.snapshots_list | ? { $nimbleVolumeList -icontains $_.vol_name } | `
    Clone-NSSnapshot -ClonePrefix $ClonePrefix -CloneSuffix $("-" + $TargetDatabase) `
    -Description "Clone of $SourceServer $database onto $TargetServer - $TargetDatabase" `
    -Metadata @{ "targetServer" = $TargetServer; "targetDatabase" = $TargetDatabase }
  Write-Log -LogLevel Information -LogMessage ("Created new clones: " + (($newClones | Select -ExpandProperty Name) -join ', '))

  Write-Log -LogLevel Information -LogMessage ("Set new clone initiator group to " + $targetInitiatorGroup.name)
  $result = $newClones | Set-NSVolumeInitiatorGroup -InitiatorGroupName $targetInitiatorGroup.name

  Write-Log -LogLevel Information -LogMessage ("Set new clones online")
  $result = $newClones | Set-NSVolume -Online

  Write-Log -LogLevel Information -LogMessage "Create iSCSI connections"
  $targetIQNs = $newClones | Select -ExpandProperty target_name
  $targetIQNs | Connect-IscsiIqn -ComputerName $ComputerName

  Write-Log -LogLevel Information -LogMessage "Mounting new volumes under $TargetCloneRoot"
  $mountList = $newClones | Mount-NimbleClone -ComputerName $ComputerName

  $cloneFileList = @()
  foreach ($sqlVolume in $sqlVolumeList) {
    $cloneFileList += ($mountList | ? { $_.NimbleVolume.parent_vol_id -eq $sqlVolume.NimbleVolume.id } | Select -ExpandProperty AccessPath) + "\" + $sqlVolume.FilePath
  }

  Write-Log -LogLevel Information -LogMessage ("Attaching $database with database files: " + ($cloneFileList -join ", "))
  Add-DatabaseClone -ServerInstance $TargetServer -Database $TargetDatabase -FileList $cloneFileList
}

<# END Main Code #>

<# BEGIN Final log #>
# Stop timer and display total execution time
$executionTime = $stopWatch.Elapsed.TotalSeconds
Write-Log -LogLevel Information "Success! Completed in $executionTime seconds"

# Send final email
if($LogEmail) { 
  $emailSubject = "Success! - $($emailSubject)"
  Send-LogEmail -smtpServer $smtpServer -emailTo $emailTo -emailFrom $emailFrom -emailSubject $emailSubject -emailBody $Script:emailBody 
}
<# END Final Code #>