﻿param
(
	[string]$winserver,
	[string]$DB
)

Add-PSSnapin VMware.VimAutomation.Core -WarningAction SilentlyContinue
Import-Module SQLPS -WarningAction SilentlyContinue

######################################################
# Variables
######################################################

$array = "cs7-fc1-test.sedemo.lab"
$username = "admin"
$password = "admin"
$parentvolume = "HSCIC"

$vcenter = "vcenter6.sedemo.lab"
$vcenter_user = "administrator@vsphere.local"
$vcenter_password = "Nim123Boli"
$vmcluster = "Cluster"
$vmhost = "esxi5-6.sedemo.lab"

$NTcredential = Import-Clixml 'c:\NTcred.xml'
$VM = $winserver.split(".")[0]


######################################################
# Detach SQL DB
######################################################

Write-Host "Detaching SQL DB $DB on $winserver `n"

$detachSQLCMD = @"
ALTER DATABASE $DB SET offline WITH ROLLBACK IMMEDIATE
GO
ALTER DATABASE $DB SET SINGLE_USER
GO
EXEC sp_detach_db @dbname = '$DB'
"@

Invoke-Sqlcmd $detachSQLCMD -QueryTimeout 3600 -ServerInstance $winserver -Username "sa" -Password "Nim123Boli"
c:

######################################################
# Remove Volume Mount Points
######################################################
Write-Host "Removing Volume Mount Points ...`n"

Invoke-Command -ComputerName $winserver -ScriptBlock { Remove-PartitionAccessPath -DiskNumber 2 -PartitionNumber 2 -AccessPath 'c:\SQLCLONE\NIMBLE\LOG' } -credential $NTcredential
Invoke-Command -ComputerName $winserver -ScriptBlock { Remove-PartitionAccessPath -DiskNumber 1 -PartitionNumber 2 -AccessPath 'c:\SQLCLONE\NIMBLE\DB' } -credential $NTcredential

######################################################
# Connect to VCenter
######################################################

Write-Host "Connecting to $vcenter ...`n"
Connect-VIServer -Server $vcenter -User $vcenter_user -Password $vcenter_password -WarningAction SilentlyContinue | Out-Null

######################################################
# Remove VMDKs
######################################################
Write-Host "Disconnecting VMDKs ...`n"

get-harddisk -vm $VM -Name 'Hard disk 3' | Remove-HardDisk -Confirm:$false | Out-Null
get-harddisk -vm $VM -Name 'Hard disk 2' | Remove-HardDisk -Confirm:$false | Out-Null

######################################################
# Delete Volumes
######################################################

.\delete-volume.ps1 -vol "HSCIC-$VM" -array $array

######################################################
# Rescan VM Cluster
######################################################

Write-Host "Rescanning Host HBAs ... `n"

get-vmhost -Name $vmhost | get-vmhoststorage -rescanallhba -rescanvmfs | Out-Null

######################################################
# Disconnect VCenter
######################################################

Disconnect-VIServer -Confirm:$false