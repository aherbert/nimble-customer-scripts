﻿param
(
	[string]$winserver,
	[string]$DB
)

Add-PSSnapin VMware.VimAutomation.Core -WarningAction SilentlyContinue
Import-Module SQLPS -WarningAction SilentlyContinue

######################################################
# Functions
######################################################

function resignature-datastore
{
	param
	(
		[string]$vmhost
	)
	
	$esxcli = Get-EsxCli -VMHost $vmhost
	$snap = $esxcli.storage.vmfs.snapshot.list() 
	$esxcli.storage.vmfs.snapshot.resignature($null, $snap.VMFSUUID) | Out-Null
	sleep 45
}

function rename-datastore
{
	param
	(
		[string]$newname
	)
	
	get-datastore | where { $_.Name -match "snap" } | Set-Datastore -Name $newname
}

function create-clone
{
	param
	(
		[string]$parent,
		[string]$clonename,
		[string]$snapid,
		[string]$array,
		[string]$token
	)
	
	$data = @{
		clone = "true"
		name = $clonename
		base_snap_id = $snapid
		online = "true"
	}
	
	$body = convertto-json (@{ data = $data })
	$header = @{ "X-Auth-Token" = $token }
	$uri = "https://" + $array + ":5392/v1/volumes"
	$result = Invoke-RestMethod -Uri $uri -Method Post -Body $body -Header $header
	#$result.data | select name, size, clone, online, parent_vol_name | format-table -autosize	
}

function get-token
{
	param
	(
		[string]$array,
		[string]$username,
		[string]$password
	)
	
	$data = @{
		username = $username
		password = $password
	}
	
	$body = convertto-json (@{ data = $data })
	
	$uri = "https://" + $array + ":5392/v1/tokens"
	$token = Invoke-RestMethod -Uri $uri -Method Post -Body $body
	$token = $token.data.session_token
	return $token
}

function get-volid
{
	param
	(
		[string]$volumename,
		[string]$array,
		[string]$token
	)
	
	$header = @{ "X-Auth-Token" = $token }
	$uri = "https://" + $array + ":5392/v1/volumes/detail?name=" + $volumename
	$volume_list = Invoke-RestMethod -Uri $uri -Method Get -Header $header
	
	return $volume_list.data.id
}

function get-snapid
{
	param
	(
		[string]$volid,
		[string]$snapname,
		[string]$array,
		[string]$token
	)
	
	$header = @{ "X-Auth-Token" = $token }
	$uri = "https://" + $array + ":5392/v1/snapshots/detail?vol_id=" + $volid + "&name=" + $snapname
	$snap = Invoke-RestMethod -Uri $uri -Method Get -Header $header
	return $snap.data.id
}

function get-latest-snapshot
{
	param
	(
		[string]$array,
		[string]$volid,
		[string]$token
	)
	
	$header = @{ "X-Auth-Token" = $token }
	$uri = "https://" + $array + ":5392/v1/snapshots/detail?vol_id=$volid"
	$snapshots = Invoke-RestMethod -Uri $uri -Method Get -Header $header
	foreach ($snapshot in $snapshots.data | Sort-Object creation_time -Descending | select -First 1)
	{
		Write-Output $snapshot.id
	}
	
}

######################################################
# Variables
######################################################

$array = "cs7-fc1-test.sedemo.lab"
$username = "admin"
$password = "admin"
$parentvolume = "HSCIC"

$vcenter = "vcenter6.sedemo.lab"
$vcenter_user = "administrator@vsphere.local"
$vcenter_password = "Nim123Boli"
$vmcluster = "Cluster"
$vmhost = "esxi5-6.sedemo.lab"

$NTcredential = Import-Clixml 'c:\NTcred.xml'

$VM = $winserver.split(".")[0]

######################################################
# Enable Self-Signed Certs for HTTPS
######################################################

[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

######################################################
# Connect to VCenter
######################################################

Write-Host "Connecting to $vcenter ...`n"

Connect-VIServer -Server $vcenter -User $vcenter_user -Password $vcenter_password -WarningAction SilentlyContinue | Out-Null

######################################################
# Create the Clone Volume
######################################################

$token = get-token -array $array -username $username -password $password
$volid = get-volid -array $array -token $token -volumename $parentvolume
#$snapid = get-snapid -array $array -token $token -volid $volid -snapname "baseline"
$snapid = get-latest-snapshot -array $array -token $token -volid $volid

Write-Host "Creating a clone of volume $parentvolume on the Nimble array $array using snap id $snapid `n"
create-clone -array $array -parent "HSCIC" -clonename "HSCIC-$VM" -snapid $snapid -token $token

######################################################
# Rescan VM Cluster
######################################################

Write-Host "Rescanning Host HBAs ... `n"

get-vmhost -Name $vmhost | get-vmhoststorage -rescanallhba -rescanvmfs | Out-Null

######################################################
# Resignature Cloned Datastore
######################################################

Write-Host "Resignaturing cloned datastore ... `n"

resignature-datastore -vmhost $vmhost | Out-Null

######################################################
# Rename Cloned Datastore
######################################################

Write-Host "Renaming Cloned Datastore to HSCIC-clone-$VM `n"

rename-datastore -newname "HSCIC-clone-$VM" | Out-Null

######################################################
# Attach DB Clone Disk to VM
######################################################

Write-Host "Attaching DB Clone VMDK ...`n"

New-HardDisk -VM $VM -DiskPath "[HSCIC-clone-$VM] FC-SQL-04/FC-SQL-04.vmdk" | Out-Null

Invoke-Command -ComputerName $winserver -ScriptBlock { set-disk -number 1 -isoffline $false } -credential $NTcredential
Invoke-Command -ComputerName $winserver -ScriptBlock { set-disk -number 1 -isreadonly $false } -credential $NTcredential
Invoke-Command -ComputerName $winserver -ScriptBlock { Add-PartitionAccessPath -DiskNumber 1 -PartitionNumber 2 -AccessPath 'c:\SQLCLONE\NIMBLE\DB' } -credential $NTcredential

######################################################
# Attach LOG Clone Disk to VM
######################################################

Write-Host "Attaching LOG Clone VMDK ...`n"

New-HardDisk -VM $VM -DiskPath "[HSCIC-clone-$VM] FC-SQL-04/FC-SQL-04_1.vmdk" | Out-Null

Invoke-Command -ComputerName $winserver -ScriptBlock { set-disk -number 2 -isoffline $false } -credential $NTcredential
Invoke-Command -ComputerName $winserver -ScriptBlock { set-disk -number 2 -isreadonly $false } -credential $NTcredential
Invoke-Command -ComputerName $winserver -ScriptBlock { Add-PartitionAccessPath -DiskNumber 2 -PartitionNumber 2 -AccessPath 'c:\SQLCLONE\NIMBLE\LOG' } -credential $NTcredential

######################################################
# Disconnect VCenter
######################################################

Disconnect-VIServer -Confirm:$false

######################################################
# Attach Cloned Database
######################################################

$mdf = 'c:\SQLCLONE\NIMBLE\DB\NIMBLE.mdf'
$ldf = 'c:\SQLCLONE\NIMBLE\LOG\NIMBLE_log.ldf'

write-host "Attaching DB:" $DB
write-host "`nDB FILE:" $mdf
write-host "LOG FILE:" $ldf

$attachSQLCMD = @"
USE [master]
GO
CREATE DATABASE [$DB] ON (FILENAME = '$mdf'),(FILENAME = '$ldf') for ATTACH
GO
"@
Invoke-Sqlcmd $attachSQLCMD -QueryTimeout 3600 -ServerInstance $winserver -Username "sa" -Password "Nim123Boli"

