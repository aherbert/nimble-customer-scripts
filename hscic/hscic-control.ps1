﻿$winservers = @("fc-sql-05.sedemo.lab", "fc-sql-06.sedemo.lab")
$DB = "NimbleClone"

foreach ($winserver in $winservers)
{
	Write-Host -ForegroundColor Yellow "`n=========================================================="
	Write-Host "Executing Script for: $winserver"
	Write-Host -ForegroundColor Yellow "==========================================================`n"
	
	.\hscic-remove-clones.ps1 -winserver $winserver -DB $DB
	.\hscic-update-clones.ps1 -winserver $winserver -DB $DB
	c:
	Write-Host "`n"
}


