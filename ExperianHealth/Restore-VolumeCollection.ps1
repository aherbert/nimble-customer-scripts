﻿
param(
  [Parameter(Mandatory=$true)]
  [string]$NimbleGroup,
  [Parameter(Mandatory=$true)]
  [string]$VolumeCollection,
  [Parameter(Mandatory=$false)]
  [int]$SnapshotNumber
)

Function Main {
  $global:NimbleGroup = $NimbleGroup

  $nimbleObj = Connect-NSGroup -NimbleGroup $NimbleGroup

  $snapColList = (Get-NSSnapshotCollection -VolumeCollection $VolumeCollection) | Sort-Object -Descending -Property creation_time

  if ($SnapshotNumber) {
    $snapColList[$SnapshotNumber - 1].snapshots_list | Restore-NSSnapshot
  }
  else {
    $global:itemNumber = 0
    $snapColList | Select-Object @{ Name="Snap#"; Expression={$global:itemNumber++; $global:itemNumber}}, Name | Format-Table -AutoSize
  }
}

Function Connect-NSGroup {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipelineByPropertyName=$true)]
    [string]$NimbleGroup=$Script:NimbleGroup
  )

  if ($Global:session_token) {
    try {
      $result = Send-NSRequest -RequestMethod get -RequestTarget "tokens/detail" -RequestArguments @{ "session_token" = $Global:session_token } -ErrorAction Stop
      return $result.data
    }
    catch {
      $Global:session_token = $null
    }
  }

  <# Get Nimble credential from registry #>
  if (!(Test-Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup")) { 
    $storedCredential = New-Item -Path "HKLM:\Software\NimbleStorage\Credentials" -Name $NimbleGroup -Force -ErrorAction Continue
  }
  else {
    $storedCredential = Get-Item -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup"
  }
  <##>

  <# Create credential object #>
  if (!$ResetPassword) {
    try {
      $nimbleUsername = (Get-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Username").Username
      $passphrase = Get-StringHash -String $($nimbleUsername + $NimbleGroup)
      $nimbleEncryptedPassword = (Get-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Password").Password
      $nimblePassword = Decrypt-String -Encrypted $nimbleEncryptedPassword -Passphrase $passphrase -salt $nimbleUsername -init $NimbleGroup | ConvertTo-SecureString -AsPlainText -Force
      $nimbleCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $nimbleUsername, $nimblePassword
    }
    catch {
      Write-Log -LogLevel Warning -LogMessage "Couldn't retrieve saved credentials"
    }
  }
  if (!$nimbleCredential -or $ResetPassword) {
    $nimbleCredential = Get-Credential -Message "Enter username and password for $NimbleGroup"
    try {
      Set-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Username" -Value $nimbleCredential.UserName
      $passphrase = Get-StringHash -String $($nimbleCredential.UserName + $NimbleGroup)
      $nimbleEncryptedPassword = Encrypt-String -String ($nimbleCredential.GetNetworkCredential().Password) -Passphrase $passphrase -salt $nimbleCredential.UserName -init $NimbleGroup
      Set-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Password" -Value $nimbleEncryptedPassword
    }
    catch {
      Write-Log -LogLevel Warning -LogMessage "Couldn't save credentials"
    }
  }
  <##>

  $scriptName = & { $MyInvocation.ScriptName } | Split-Path -Leaf
  $networkCredential = $nimbleCredential.GetNetworkCredential()
  $result = Send-NSRequest -RequestMethod Post -RequestTarget "tokens" -RequestArguments @{ 
    username = $networkCredential.UserName
    password = $networkCredential.Password
    app_name = $scriptName
  } -ErrorAction SilentlyContinue
  if (!$result.data.session_token -and !$ResetPassword) {
    $Script:ResetPassword = $true
    return Connect-NSGroup $NimbleGroup
  }
  else {
    $Global:session_token = $result.data.session_token
    return $result.data
  }
}

Function Send-NSRequest {
<#
.SYNOPSIS
Abstract the Invoke-Restmethod for all other module cmdlets

.DESCRIPTION
.PARAMETER RequestMethod
  Set the request method used. Valid options are 'get', 'delete', 'post', 'put'
.PARAMETER RequestTarget
  Set the request object and any sub object
.PARAMETER RequestArguments
  Set URI arguments for GET requests
.PARAMETER RequestHeaders
  Set any additional headers. The X-Auth-Token header is set automatically from the $global:session_token variable
.PARAMETER RequestBody
  Set the Request Body. 
.INPUTS
.OUTPUTS
.EXAMPLE
.EXAMPLE
.LINK
#>
  [CmdletBinding()]
  Param
  (
    [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
    [ValidateSet('get', 'delete', 'post', 'put')]
    [string]$RequestMethod,

    [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
    [string]$RequestTarget,
        
    [Parameter(Mandatory=$false,ValueFromPipelineByPropertyName=$true)]
    [hashtable]$RequestArguments,

    [Parameter(Mandatory=$false,ValueFromPipelineByPropertyName=$true)]
    [hashtable]$RequestHeaders = @{},
        
    [Parameter(Mandatory=$false,ValueFromPipelineByPropertyName=$true)]
    [string]$NimbleGroup=$Script:NimbleGroup
  )

  if ( ![string]::IsNullOrEmpty($Global:session_token) ) { $RequestHeaders.Add("X-Auth-Token", $Global:session_token) }

  $uri = 'https://' + $NimbleGroup + ':5392/'
  if ($RequestTarget -eq 'versions') {
    $uri = $uri + 'versions'
  }
  else {
    $uri = $uri + 'v1/' + $RequestTarget
  }

  if ( $RequestMethod -ieq 'get' ) {
    if ($RequestArguments.Count -gt 0) {
      $uri += '?'
      $uri += [string]::join("&", @(foreach($pair in $RequestArguments.GetEnumerator()) { 
        if ($pair.Name) { 
          $pair.Name + '=' + $pair.Value 
        } 
      }))
    }
  }
  else {
    $RequestJSON = @{ data = $RequestArguments } | ConvertTo-Json -Depth 5
  }

  try {
    $request = [System.Net.WebRequest]::Create($uri)
    $request.Method = $RequestMethod.ToUpper()
    $request.ContentType = "application/json"
    $RequestHeaders.GetEnumerator() | % { $request.Headers.Add($_.Key, $_.Value) }
    if ( $RequestMethod -ine 'GET' -and $RequestArguments.Count -gt 0 ) {
      $requestStream = $request.GetRequestStream();
      $streamWriter = New-Object System.IO.StreamWriter $requestStream
      $streamWriter.Write($RequestJSON.ToString())
      $streamWriter.Flush()
      $streamWriter.Close()
    }
 
    $response = $request.GetResponse()
  }
  catch {
    $httpStatus = [regex]::matches($_.exception.message, "(?<=\()[\d]{3}").Value
    if ( $httpStatus -eq '401' ) {
      Write-Error 'You must login into the Nimble Array first'
    }
    elseif ($httpStatus -ne $null) {
      $responseStream = $_.Exception.InnerException.Response.GetResponseStream()
      $readStream = New-Object System.IO.StreamReader $responseStream
      $data = $readStream.ReadToEnd()
      $results = $data | ConvertFrom-Json
      Write-Error "$('There was an error, status code: ' + $httpStatus)`n$($uri)`n$($RequestJSON.ToString())`n$($results.messages | Format-Table | Out-String)"
    }
    else { 
      Write-Error $_.Exception
    }
    Return
  }
  $responseStream = $response.GetResponseStream()
  $readStream = New-Object System.IO.StreamReader $responseStream
  $data = $readStream.ReadToEnd()
  $results = New-Object -TypeName PSCustomObject
  $results | Add-Member -MemberType NoteProperty -Name StatusCode -Value "$([int]$response.StatusCode) - $($response.StatusCode)" 
  $results | Add-Member -MemberType NoteProperty -Name data -Value $($data | ConvertFrom-Json).data

  Return $results
}

Function Get-NSSnapshotCollection {
  param(
    [Parameter(Mandatory=$false)]
    [string]$VolumeCollection,
    [Parameter(Mandatory=$false)]
    [string]$SnapCollectionId,
    [Parameter(Mandatory=$false)]
    [hashtable]$RequestArgument=@{}
  )
  if ($SnapCollectionId) {
    $requestTarget = "snapshot_collections/$SnapCollectionId"
  }
  else {
    $requestTarget = "snapshot_collections/detail"
    if ($VolumeCollection) { $RequestArgument.volcoll_name = $VolumeCollection }
  }
  (Send-NSRequest -RequestMethod get -RequestTarget $requestTarget -RequestArguments $RequestArgument).data
}

Function Restore-NSSnapshot {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipeline=$true)]
    [System.Object[]]$SnapshotObject
  )

  Process {
    $request = @{
      base_snap_id = $SnapshotObject.snap_id
    }
    
    $result = Set-NSVolume -VolumeId $SnapshotObject.vol_id -Online:$false -Force
    $result = Send-NSRequest -RequestMethod post -RequestTarget "volumes/$($SnapshotObject.vol_id)/actions/restore" -RequestArguments $request
    $result = Set-NSVolume -VolumeId $SnapshotObject.vol_id -Online:$true
  }
}

Function Get-NSVolume {
  param(
    [Parameter(Mandatory=$false)]
    [string]$VolumeID,
    [Parameter(Mandatory=$false)]
    [string]$Volume,
    [Parameter(Mandatory=$false)]
    [hashtable]$RequestArgument=@{}
  )

  if ($VolumeID) {
    $requestTarget = "volumes/$VolumeID"
  }
  else {
    $requestTarget = "volumes/detail"
    if ($Volume -ne "") { $RequestArgument.name = $Volume }
  }
  (Send-NSRequest -NimbleGroup $NimbleGroup -RequestMethod get -RequestTarget $requestTarget -RequestArguments $RequestArgument).data
}

Function Set-NSVolume {
  param(
    [Parameter(Mandatory=$false,
    ValueFromPipeline=$true)]
    [System.Object[]]$VolumeObject,
    [Parameter(Mandatory=$false)]
    [string]$VolumeId,
    [Parameter(Mandatory=$false)]
    [switch]$Online,
    [Parameter(Mandatory=$false)]
    [switch]$Force,
    [Parameter(Mandatory=$false)]
    [hashtable]$RequestArgument=@{}
  )

  Process {
    if ($VolumeObject.id -eq $null) {
      $VolumeObject = Get-NSVolume -VolumeId $VolumeId
    }

    $RequestArgument.online = $Online.ToString().ToLower()
    $RequestArgument.force = $Force.ToString().ToLower()

    $result = Send-NSRequest -RequestMethod put -RequestTarget $("volumes/" + $VolumeObject.id) -RequestArguments $RequestArgument
    $result.data
  }
}

Main